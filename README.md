Проект - Project name
---

Краткое описание проекта - предметная область.

---

## Ссылки

- [Figma](https://www.figma.com/)
- [Репозиторий фронта](https://gitlab.com/)
- [Тестовый домен](https://rocketfirm.net/)
- [Боевой домен](https:/rocketfirm.net/)

---
## Tech Stack

### Frontend

### Backend

- [Yii 2](https://www.yiiframework.com/) – PHP Framework
- [MariaDB](https://mariadb.org/) - СУБД (версия 10.2.33)

---

## Команда проекта

✅ – активный, ⛔️ – не активный, ✴️ – временный

#### PM

- PM – [Telegram](https://telegram.me/PM) ✅

#### Designer

- Designer - [Telegram](https://telegram.me/Designer) ✅

#### Art-direction

- Art-direction – [Telegram](https://telegram.me/Art-direction) ✅

#### Frontend

- Frontend – [Telegram](https://telegram.me/Frontend), [GitLab](https://gitlab.com/Frontend) ✅

#### Backend

- Backend – [Telegram](https://telegram.me/Backend), [GitLab](https://gitlab.com/Backend) ✅

---

Установка Backend:
-------------------

~~~
composer create-project rocketfirmcom/yii2-app-advanced
~~~

Запустить инициализацию проекта: 
~~~
php init
~~~

Запустить миграции: 
~~~
php yii migrate
php yii_test migrate
~~~

Создать пользователя:
~~~
php yii user/create admin@sitename.kz admin PASSWORD admin
~~~

Запуск тестов (Перед запуском нужно создать тестовую бд и прописать её в common\config\test_local.php):
~~~
vendor\bin\codecept.bat run
vendor\bin\codecept.bat run -- -c common
vendor\bin\codecept.bat run -- -c backend
vendor\bin\codecept.bat run -- -c frontend
~~~

Интернационализация - генерировать переводы c с консоли

`php yii message common/config/translate_messages_config.php`

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both admin and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
admin
    assets/              contains application assets such as JavaScript and CSS
    config/              contains admin configurations
    controllers/         contains Web controller classes
    models/              contains admin-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for admin application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
api
    config               contains API configurations
    controllers          contains API controller classes
    runtime              contains files generated during runtime
    web                  contains the entry script and API resources
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
