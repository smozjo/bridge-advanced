<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 19.10.2019
 * Time 14:50
 */

namespace common\components;


class Environment extends \janisto\environment\Environment
{
	protected function getConfig()
	{

		$configMerged = [];
		foreach ($this->configDir as $configDir) {
			// Merge main config.
			if(!$yiiEnd = getenv('YII_END')) {
				$yiiEnd = 'main';
			}
			$fileMainConfig = $configDir . $yiiEnd . '.php';

			if (!file_exists($fileMainConfig)) {
				throw new \Exception('Cannot find main config file "' . $fileMainConfig . '".');
			}
			$configMain = require($fileMainConfig);
			if (is_array($configMain)) {
				$configMerged = static::merge($configMerged, $configMain);
			}

			// Merge mode specific config.
			$fileSpecificConfig = $configDir . 'mode_' . $this->mode . '.php';
			if (!file_exists($fileSpecificConfig)) {
				throw new \Exception('Cannot find mode specific config file "' . $fileSpecificConfig . '".');
			}
			$configSpecific = require($fileSpecificConfig);
			if (is_array($configSpecific)) {
				$configMerged = static::merge($configMerged, $configSpecific);
			}

			// If one exists, merge local config.
			$fileLocalConfig = $configDir . 'local.php';
			if (file_exists($fileLocalConfig)) {
				$configLocal = require($fileLocalConfig);
				if (is_array($configLocal)) {
					$configMerged = static::merge($configMerged, $configLocal);
				}
			}
		}

		return $configMerged;
	}
}