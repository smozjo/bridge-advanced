<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 08.10.2019
 * Time 12:53
 */

namespace common\components\behaviors;


use common\helpers\LanguageHelper;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class AutoFillTranslationForm extends Behavior
{
	private $mainLanuageCode;
	public $excludeAttributes = [];

	public function init()
	{
		parent::init();

		$this->mainLanuageCode = array_key_first(LanguageHelper::getLanguages());
	}

	public function events()
	{
		if(\Yii::$app->request->isAjax) {
			return [];
		}
		return [
			ActiveRecord::EVENT_BEFORE_INSERT => 'save',
			ActiveRecord::EVENT_BEFORE_UPDATE => 'save',
		];
	}

	public function save()
	{
		$formName = $this->owner->translation->formName();
		$data = \Yii::$app->request->getBodyParam($formName);

		$mainData = ArrayHelper::getValue($data, $this->mainLanuageCode);
		if(!$data || !$mainData) {
			return false;
		}

		foreach ($data as $langCode => $datum) {
			foreach ($datum as $key => $value) {
				if(in_array($key, $this->excludeAttributes)) {
					continue;
				}
				$data[$langCode][$key] = $value ?: $data[$this->mainLanuageCode][$key];
			}
		}

		$bodyParams = \Yii::$app->request->getBodyParams();

		$bodyParams[$formName] = $data;

		\Yii::$app->request->setBodyParams($bodyParams);
	}


}