<?php

namespace common\components\behaviors;

use common\components\admin\AjaxUploader;
use mohorev\file\UploadImageBehavior;
use yii\base\InvalidArgumentException;
use yii\db\BaseActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class BridgeUploadImageBehavior extends \Bridge\Core\Behaviors\BridgeUploadImageBehavior
{
	/** @var AjaxUploader */
	public $ajaxUploader;

    public function beforeSave()
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;

		if($this->ajaxUploader && $this->ajaxUploader->isFileExist()) {
			$fileName = sprintf('%s.%s',
				uniqid(),
				$this->ajaxUploader->getFileInfo()->extension
			);

			$model->setAttribute($this->attribute, $fileName);
		} else {
			if (in_array($model->scenario, $this->scenarios)) {
				if ($this->_file instanceof UploadedFile) {
					if (!$model->getIsNewRecord() && $model->isAttributeChanged($this->attribute)) {
						if ($this->unlinkOnSave === true) {
							$this->delete($this->attribute, true);
						}
					}
					$model->setAttribute($this->attribute, $this->_file->name);
				} else {
					if($model->hasProperty($this->attribute)) {
						unset($model->{$this->attribute});
					}
				}
			} else {
				if (!$model->getIsNewRecord() && $model->isAttributeChanged($this->attribute)) {
					if ($this->unlinkOnSave === true) {
						$this->delete($this->attribute, true);
					}
				}
			}
		}
	}

    public function afterSave()
	{
		if($this->ajaxUploader && $this->ajaxUploader->isFileExist()) {
			$destination = \Yii::getAlias($this->resolvePath($this->path));

			FileHelper::createDirectory($destination);

			$this->ajaxUploader->moveTo(sprintf('%s/%s',
				$destination,
				$this->owner->{$this->attribute}
			));

			$this->ajaxUploader->removeDestination();
			$this->afterUpload();
		} else {
			parent::afterSave();
		}
	}
}