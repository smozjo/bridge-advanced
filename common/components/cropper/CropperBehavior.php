<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 28.02.2020
 * Time 19:00
 */

namespace common\components\cropper;


use common\components\helpers\FileHelper;
use mohorev\file\UploadImageBehavior;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;

class CropperBehavior extends UploadImageBehavior
{
	public $attribute;
	public $uploadPath;
	public $thumbs = [];
	public $fileName;
	public $prefix;

	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
			ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
			ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
		];
	}

	public function beforeSave()
	{
		$uploadPath = \Yii::getAlias(str_replace(
			'{id}',
				(int)$this->owner->id,
			$this->uploadPath)
		);
		FileHelper::createDirectory($uploadPath);

		$result = FileHelper::saveImageFromBase64(
			$this->owner->{$this->attribute},
			$uploadPath,
			$this->fileName,
			$this->prefix
		);

		$basename = ArrayHelper::getValue($result, 'basename');
		$extension = ArrayHelper::getValue($result, 'extension');

		if(!$basename) {
			return;
		}

		foreach ($this->thumbs as $prefix => $thumb) {
			$this->generateImageThumb(
				$thumb,
				$uploadPath . '/' . $basename,
				$uploadPath . '/' . $this->getThumbFileName($basename, $prefix)
			);
		}

		if($this->fileName) {
			$basename = $this->fileName . '.' . $extension;
		}
		$this->owner->{$this->attribute} = $basename;

		return true;
	}

	public function afterSave()
	{
		$currentUploadPath = \Yii::getAlias(str_replace(
			'{id}',
			0,
			$this->uploadPath)
		);

		$distUploadPath = \Yii::getAlias(str_replace(
			'{id}',
			(int)$this->owner->id,
			$this->uploadPath)
		);

		FileHelper::createDirectory($distUploadPath);

		$baseName = $this->owner->{$this->attribute};

		rename(
			$currentUploadPath . '/' . $this->prefix . $baseName,
			$distUploadPath . '/' . $this->prefix . $baseName
		);

		/*
		foreach ($this->thumbs as $prefix => $thumb) {
			$baseName = $this->getThumbFileName($this->owner->{$this->attribute}, $prefix);
			rename(
				$currentUploadPath . '/' . $baseName,
				$distUploadPath . '/' . $baseName
			);
		}
		*/
	}

	public function getThumbUploadUrl($attribute, $profile = 'thumb')
	{
		/** @var BaseActiveRecord $model */
		$model = $this->owner;

		if ($this->createThumbsOnRequest) {
			$this->createThumbs();
		}

		if (is_file($this->getThumbUploadPath($attribute, $profile))) {
			$url = $this->resolvePath($this->thumbUrl);
			$fileName = $model->$attribute;
			$thumbName = $this->getThumbFileName($fileName, $profile);

			return \Yii::getAlias($url . '/' . $thumbName);
		} elseif ($this->placeholder) {
			return $this->getPlaceholderUrl($profile);
		} else {
			return null;
		}
	}

}