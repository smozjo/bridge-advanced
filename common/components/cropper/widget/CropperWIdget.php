<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 28.02.2020
 * Time 18:14
 */

namespace common\components\cropper\widget;


use yii\base\Widget;

class CropperWIdget extends Widget
{
	public $id;
	public $form;
	public $model;
	public $attribute;
	public $width;
	public $height;
	public $cropWidth;
	public $cropHeight;
	public $prefix;

	public function run()
	{
		\common\assets\CropperAsset::register($this->view);

		return $this->render('cropper', [
			'id' => $this->id,
			'form' => $this->form,
			'model' => $this->model,
			'attribute' => $this->attribute,
			'width' => $this->width,
			'height' => $this->height,
			'cropWidth' => $this->cropWidth,
			'cropHeight' => $this->cropHeight,
			'prefix' => $this->prefix
		]);
	}
}