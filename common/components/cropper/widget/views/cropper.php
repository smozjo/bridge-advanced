<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 28.02.2020
 * Time 18:16
 */

/** @var \yii\db\ActiveRecord $model */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var string $attribute */
/** @var string $width */
/** @var string $height */
/** @var string $cropWidth */
/** @var string $cropHeight */
/** @var string $id */

$this->registerJs('$(\'.cropme\').simpleCropper();');

$this->registerCss(
	<<<CSS
    .widget-panel {   
        background-color: #f5f5f5;
        padding: 10px;
        border: 1px solid #ddd
    }
    
     .widget-panel .row{
        padding:10px;
     }
CSS

);

$field = $form->field($model, $attribute)->hiddenInput();

?>
<div class="panel widget-panel">
	<div class="panel panel-default">
		<div class="tab-content">
			<div class="row">
				<div class="col-md-12">
					<?= $field; ?>
					<div
                        id="<?= $id; ?>"
						class="cropme"
						style="min-width: <?= $width; ?>px; min-height: <?= $height?>px; width: 100%"
						data-width="<?= $cropWidth?>"
						data-height="<?= $cropHeight?>"
						data-control-id="<?= \yii\helpers\Html::getInputId($model, $attribute)?>"
					>
                        <?php if($model->$attribute):?>
                            <?php if($prefix): ?>
                                <img src="<?= $model->getThumbUploadUrl($attribute, $prefix); ?>?hash=<?= microtime(); ?>">
							<?php else: ?>
                                <img src="<?= $model->getUploadUrl($attribute); ?>?hash=<?= microtime(); ?>">
							<?php endif; ?>
                        <?php endif; ?>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>



