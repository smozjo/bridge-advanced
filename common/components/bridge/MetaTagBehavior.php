<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.01.2020
 * Time 15:23
 */

namespace common\components\bridge;


use Bridge\Core\Models\MetaModel;

class MetaTagBehavior extends \Bridge\Core\Behaviors\MetaTagBehavior
{
	public $className;

	public function saveMetaTags()
	{
		if(!$this->owner->metaModel) {
			MetaModel::create($this->owner, null);

			if($this->className) {
				$metaModel = MetaModel::findOne(['model' => get_class($this->owner), 'model_id' => $this->owner->id]);
				$metaModel->model = $this->className;
				$metaModel->save();
			}
		}

		return $this->owner->metaTag->save();
	}

	public function getMetaModel()
	{
		return MetaModel::findOne(['model' => $this->className, 'model_id' => $this->owner->id]);
	}

}