<?php

namespace common\components\bridge;

use common\components\interfaces\ParentClassInterface;
use Bridge\Core\Behaviors\BridgeUploadImageBehavior;
use Bridge\Core\Models\MetaModel;
use Bridge\Core\Models\MetaTagTranslation;
use Yii;
use yii\db\ActiveRecord;

use yii\helpers\Url;

class MetaTagsComponent extends \Bridge\Core\Components\MetaTagsComponent
{
    public function registerModel(ActiveRecord $model, string $metaTagBehaviorName = 'metaTag', string $imageUploadBehaviorName = 'imageUpload')
    {
		$cacheName = 'meta-model-' . md5(get_class($model) . $model->id);

		$metaModel = \Yii::$app->getCache()->get($cacheName);

		if ($metaModel === false) {
			if(!$model instanceof ParentClassInterface) {
				$metaModel = MetaModel::getOrCreate($model, $metaTagBehaviorName);
			} else {
				$metaModel = MetaTagTranslation::find()
					->joinWith('metaTag.metaModel', false)
					->where([
						'meta_models.model' => $model->getMainModelclass,
						'meta_models.model_id' => $model->primaryKey,
						'meta_tag_translations.lang' => Yii::$app->language
					])
					->one();
			}
			\Yii::$app->getCache()->set($cacheName, $metaModel);
		}

		if ($metaModel) {
            $this->registerMetaTitle($metaModel->title);
            $this->registerMetaDescription($metaModel->description);
            $this->registerMetaUrl();
            $this->registerMetaSiteName();
            $this->registerMetaImage($this->getModelMetaImage($metaModel, $model, $imageUploadBehaviorName));
        }
    }

	private function getModelMetaImage(MetaTagTranslation $metaTagTranslationModel, ActiveRecord $model, string $imageUploadBehaviorName)
	{
		$image = $metaTagTranslationModel->getUploadUrl('image');
		if (!is_null($image)) {
			return Url::to($image, true);
		}

		/** @var BridgeUploadImageBehavior $imageUploadBehavior */
		$imageUploadBehavior = $model->getBehavior($imageUploadBehaviorName);
		if (!is_null($imageUploadBehavior) && (get_class($imageUploadBehavior) === BridgeUploadImageBehavior::class)) {
			$image = $model->getUploadUrl($imageUploadBehavior->attribute);
			if (!is_null($image)) {
				return Url::to($image, true);
			}
		}

		return $this->defaultMetaImage;
	}
}