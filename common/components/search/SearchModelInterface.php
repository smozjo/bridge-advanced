<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 22.10.2019
 * Time 15:27
 */

namespace common\components\search;


use yii\db\ActiveQuery;

/**
 * Interface SearchModelInterface
 * @package app\api\modules\v1\components\search
 */
interface SearchModelInterface
{
	/**
	 * @return array|null
	 */
	public function getResult(): ?array ;

	/**
	 * @return int
	 */
	public function getCount(): int ;

	/**
	 * @return int
	 */
	public function getTotalCount(): int ;

	/**
	 * @param array $params
	 * @param bool $absolute
	 * @return array
	 */
	public function getLinks(array $params = [], bool $absolute = false): array ;

	/**
	 * @param int $limit
	 * @return $this
	 */
	public function setLimit(int $limit): self ;

	/**
	 * @return int
	 */
	public function getLimit(): int;

	/**
	 * @param int $page
	 * @return $this
	 */
	public function setPage(int $page): self ;

	/**
	 * @return int
	 */
	public function getPage(): int ;
}