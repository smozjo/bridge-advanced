<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 22.10.2019
 * Time 15:28
 */

namespace common\components\search;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

abstract class SearchModelAbstract extends Model implements SearchModelInterface
{
	/**
	 * @var string
	 */
	protected static $modelClass;
	/**
	 * @var int
	 */
	protected $limit  = 10;
	/**
	 * @var int
	 */
	protected $page = 1;
	/**
	 * @var int
	 */
	protected $totalCount = 0;
	/**
	 * @var array
	 */
	protected $result = [];
	/**
	 * @var int
	 */
	protected $count = 0;

	/**
	 * @param int $limit
	 * @return SearchModelInterface
	 */
	public function setLimit(int $limit): SearchModelInterface
	{
		$this->limit = $limit;

		return $this;
	}

    public function getLimit(): int
    {
        return $this->limit;
    }

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		if(empty($this->count)) {
            $this->count = count($this->getResult());
		}

		return $this->count;
	}

	/**
	 * @return int
	 */
	public function getTotalCount(): int
	{
		if(empty($this->totalCount)) {
			$this->totalCount = $this->query()->count();
		}

		return $this->totalCount;
	}

	/**
	 * @param array $params
	 * @param bool $absolute
	 * @return array
	 */
	public function getLinks(array $params = [], bool $absolute = false): array
	{
		return (new Pagination([
			'totalCount' => $this->getTotalCount(),
			'defaultPageSize' => $this->limit
		]))->getLinks($absolute);
	}

	/**
	 * @param int $page
	 * @return SearchModelInterface
	 */
	public function setPage(int $page): SearchModelInterface
	{
		$this->page = $page;

		return $this;
	}

    public function getPage(): int
    {
        return $this->page;
    }

	/**
	 * @return array
	 */
	public function getResult(): array
	{
		if($this->getTotalCount()) {
			$this->result = $this->query()
				->limit($this->limit)
				->offset(($this->page-1)*$this->limit)
				->all();
		}

		return $this->result;
	}

    /**
     * @return array
     */
    public function getRandResult(): array
    {
        if($this->getTotalCount()) {
            $offset = rand(1, $this->getTotalCount() - $this->limit );
            $this->result = $this->query()
                ->limit($this->limit)
                ->offset($offset)
                ->all();
        }

        return $this->result;
    }

	/**
	 * @return ActiveQuery
	 */
	abstract protected function query(): ActiveQuery ;
}