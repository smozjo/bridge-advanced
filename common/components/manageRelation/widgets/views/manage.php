<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 08.10.2019
 * Time 10:45
 */

use yii\helpers\Html;

/** @var string $tableId */
/** @var string $repeaterId */
/** @var string $repeaterTitle */
/** @var string $className */
/** @var string $repeaterView */
/** @var string $prevAction */
/** @var string $nextAction */
/** @var string $deleteAction */
/** @var string $toggleAction */
/** @var string $repeaterBtnTitle */
/** @var string $buttonsTemplate */
/** @var array $additionalData */
/** @var \yii\widgets\ActiveForm $form*/
/** @var \yii\db\ActiveQuery $dataProviderQuery */

$this->registerJs(
	<<<JS
		window.initEdit = function(id) {
    		$('#vp-$repeaterId .additional-client-data').val(id);
    		$('#vp-$repeaterId .new-item').click();
    		$('#vp-$repeaterId .additional-client-data').val('');
		};
		window.deleteSeries = function(url) {
    		wjax.doit(url, {}, function(){
				$.pjax.reload({container:"#$tableId"});        
			}, 'POST');
		}
JS

);

$isActive = [];



if($isActive = array_search('is_active', $gridColumns)) {
	$gridColumns[$isActive] = [
		'attribute' => 'is_active',
		'value' => function($model) use($toggleAction, $tableId) {
			return \yii\helpers\Html::a(
				\yii\helpers\Html::tag('span', '', ['class' => 'glyphicon glyphicon-remove-circle btn btn-' . ($model->is_active ? 'success' : 'default')]),
				'javascript:wjax.doit(\'' .  \yii\helpers\Url::to([
					$toggleAction,
					'attribute' => 'is_active',
					'id' => $model->id
				]) . '\', {}, function(){
                                $.pjax.reload({container:\'#' . $tableId . '\'});
                            })',
				['class' => 'is_active_toggle']
			);
		},
		'format' => 'raw'
	];
}

if($position = array_search('position', $gridColumns)) {
	$gridColumns[$isActive] = [
		'header' => 'Позиция',
		'class' => 'yii2tech\admin\grid\PositionColumn',
		'value' => 'position',
		'template' => '<div class="btn-group">{prev}&nbsp;{next}</div>',
		'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
		'urlCreator' => function ($action, $model, $key, $index) use($tableId, $prevAction, $nextAction) {
			if ($action === 'prev') {
				return 'javascript:wjax.doit(\'' . \yii\helpers\Url::to([
						$prevAction,
						'id' => $model->id,
						'at' => 'prev'
					]) . '\', {}, function(data){
							    $.pjax.reload({container:\'#' . $tableId . '\'});
							})';
			} elseif($action == 'next') {
				return 'javascript:wjax.doit(\'' . \yii\helpers\Url::to([
						$nextAction,
						'id' => $model->id,
						'at' => 'next'
					]) . '\', {}, function(data){
							    $.pjax.reload({container:\'#' . $tableId . '\'});
							})';
			}
		},
	];
}


$columns = \yii\helpers\ArrayHelper::merge(
	[['class' => 'yii\grid\SerialColumn']],
	$gridColumns,
	[
		[
			'class' => \common\components\admin\RFAActionColumn::class,
			'template' => $buttonsTemplate,
			'buttons' => [
				'rm' => function ($url, $model) use($deleteAction) {
					return Html::a('<span class="glyphicon glyphicon-trash"></span>', 'javascript:deleteSeries("' . \yii\helpers\Url::to([
							$deleteAction,
							'id' => $model->id
						]) . '")', [
						'title' => \Yii::t('app', 'Delete'),
						'data-pjax' => '0',
						'data-confirm' => \Yii::t('app', 'Are you sure you want to delete this item?'),
						'class' => 'btn btn-danger'
					]);
				}
			],
			'urlCreator' => function ($action, $model, $key, $index) {
				if ($action === 'update') {
					return 'javascript:initEdit(' . $model->id . ');';
				}
			}
		],
	]);

?>

<?php \yii\widgets\Pjax::begin(['id' => $tableId])?>

<?= \kartik\grid\GridView::widget([
	'dataProvider' => new \yii\data\ActiveDataProvider([
		'query' => $dataProviderQuery,
		'pagination' => [
			'pageSize' => 10
		]

		//->select('services.*')
		//->addSelect(['quantity' => 'requests_services.quantity'])
	]),
	'options' => ['class' => 'grid-view table-responsive'],

	'columns' => $columns,
]); ?>
<?php \yii\widgets\Pjax::end(); ?>




<?php $this->beginContent('@app/views/layouts/admin/_panel.php', ['title' => \Yii::t('app', $repeaterTitle)]) ?>

<?= \prokhonenkov\repeater\widgets\RepeaterWidget::widget([
	'id' => $repeaterId,
	'className' => $className,
	'modelView' => $repeaterView,
	'models' => [],
	'additionalData' => $additionalData,
	'btnNewTitle' => \Yii::t('app', $repeaterBtnTitle),
	'form' => $form,
	'addCallback' => new \yii\web\JsExpression("
                    function(data){
                       
                    }
                "),
	'removeCallback' => new \yii\web\JsExpression("
                    function(data){
                     
                    }
                ")
])?>

<?php $this->endContent() ?>
