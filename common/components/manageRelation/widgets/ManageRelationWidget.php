<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 08.10.2019
 * Time 10:43
 */

namespace common\components\manageRelation\widgets;


use common\assets\AjaxAsset;
use yii\base\Widget;

class ManageRelationWidget extends Widget
{
	public $tableId;
	public $repeaterId;
	public $repeaterTitle = 'Добавление/Редактирование';
	public $repeaterBtnTitle;
	public $className;
	public $repeaterView;
	public $prevAction;
	public $nextAction;
	public $deleteAction;
	public $toggleAction;
	public $form;
	public $dataProviderQuery;
	public $gridColumns = [];
	public $buttonsTemplate = '{update}{rm}';
	public $additionalData = [];

	public function run()
	{
		AjaxAsset::register($this->view);
		return $this->render('manage', [
			'tableId' => $this->tableId,
			'repeaterId' => $this->repeaterId,
			'className' => $this->className,
			'repeaterView' => $this->repeaterView,
			'prevAction' => $this->prevAction,
			'nextAction' => $this->nextAction,
			'deleteAction' => $this->deleteAction,
			'toggleAction' => $this->toggleAction,
			'form' => $this->form,
			'dataProviderQuery' => $this->dataProviderQuery,
			'repeaterBtnTitle' => $this->repeaterBtnTitle,
			'gridColumns' => $this->gridColumns,
			'buttonsTemplate' => $this->buttonsTemplate,
			'repeaterTitle' => $this->repeaterTitle,
			'additionalData' => $this->additionalData
		]);
	}
}