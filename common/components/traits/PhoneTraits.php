<?php

namespace common\components\traits;

trait PhoneTraits
{
    public function normalizePhone($value) {
        return preg_replace('~\D~','',$value);
    }

    public function removeCountryCode($value) {
        return substr (self::normalizePhone($value), -10);
    }
}