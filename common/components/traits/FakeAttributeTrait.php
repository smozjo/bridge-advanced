<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 08.10.2019
 * Time 10:19
 */

namespace common\components\traits;


trait FakeAttributeTrait
{
	private function fakeAttribute($name)
	{
		foreach ($this->fakeAttributes as $fakeAttribute) {
			if(preg_match('/' . $this->formName() . '\[[\d{1,}]\]\[' . $fakeAttribute . '\]/', $name)) {
				return $fakeAttribute;
			}
		}

		return $name;
	}

	public function setAttribute($name, $value)
	{
		parent::setAttribute($this->fakeAttribute($name), $value);
	}

	public function getOldAttribute($name)
	{
		return parent::getOldAttribute($this->fakeAttribute($name));
	}

	public function __get($name)
	{
		return parent::__get($this->fakeAttribute($name));
	}
}