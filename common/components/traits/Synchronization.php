<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 11.10.2019
 * Time 17:33
 */

namespace common\components\traits;


use app\modules\movies\models\Movies;
use app\modules\movies\models\MoviesPersons;
use app\modules\persons\models\Persons;
use app\modules\persons\models\PersonsFilmography;

trait Synchronization
{
	public function synchronizationPersonsFilms(): bool
	{
		$transaction = \Yii::$app->getDb()->beginTransaction();

		if($this->class == Persons::class) {
			MoviesPersons::deleteAll([
				'person_id' => $this->id
			]);

			foreach ($this->personsMoviesFilmography as $movie) {
				$personsMovie = new MoviesPersons();
				$personsMovie->load($movie->attributes, '');
				if(!$personsMovie->save()) {
					$transaction->rollBack();
					return false;
				}
			}
		} elseif(is_subclass_of($this, Movies::class)) {

			PersonsFilmography::deleteAll([
				'movie_id' => $this->id
			]);

			foreach ($this->moviesPersons as $movie) {
				$filmography = new PersonsFilmography();
				$filmography->load($movie->attributes, '');
				$filmography->year = $this->year;
				if(!$filmography->save()) {
					$transaction->rollBack();
					return false;
				}
			}
		}


		$transaction->commit();
		return true;
	}
}