<?php

namespace common\components\taggable;


use dosamigos\taggable\Taggable;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class RFTaggable extends Taggable
{
	public $allowFind = false;

	public function events()
	{
		return ArrayHelper::merge(parent::events(), [ActiveRecord::EVENT_AFTER_FIND => 'afterFind']);
	}

    public function afterFind($event)
    {
    	if(!$this->allowFind) {
    		return false;
		}
		$items = [];

		foreach ($this->owner->{$this->relation} as $tag) {
			$items[] = $tag->{$this->name};
		}

		$this->owner->{$this->attribute} = is_array($this->owner->{$this->attribute})
			? $items
			: implode(', ', $items);

    }

	public function afterSave($event)
	{
		if ($this->tagValues === null) {
			$this->tagValues = $this->owner->{$this->attribute};
		}

		if (!$this->owner->getIsNewRecord()) {
			$this->beforeDelete($event);
		}

		$names = array_unique(preg_split(
			'/\s*,\s*/u',
			preg_replace(
				'/\s+/u',
				' ',
				is_array($this->tagValues)
					? implode(',', $this->tagValues)
					: $this->tagValues
			),
			-1,
			PREG_SPLIT_NO_EMPTY
		));

		$relation = $this->owner->getRelation($this->relation);
		$pivot = $relation->via->from[0];
		/** @var ActiveRecord $class */
		$class = $relation->modelClass;
		$rows = [];
		$updatedTags = [];

		foreach ($names as $name) {
			$name = ucfirst($name);
			$tag = $class::findOne([$this->name => $name]);

			if ($tag === null) {
				$tag = new $class();
				$tag->{$this->name} = $name;
			}

			$tag->{$this->frequency}++;

			if ($tag->save()) {
				$updatedTags[] = $tag;
				$rows[] = [$this->owner->getPrimaryKey(), $this->owner->getTagType(), $tag->getPrimaryKey()];
			}
		}

		if (!empty($rows)) {
			$this->owner->getDb()
				->createCommand()
				->batchInsert(
					$pivot,
					ArrayHelper::merge(
						array_keys($relation->via->link),
						array_keys($relation->via->on), [current($relation->link)]
					),
					$rows
				)->execute();
		}

		$this->owner->populateRelation($this->relation, $updatedTags);
	}

}
