<?php
/**
 * Created by PhpStorm.
 * User: prokhonenkov
 * Date: 18.02.19
 * Time: 10:53
 */

namespace common\components\taggable;

use yii\db\ActiveQuery;

/**
 * Interface RFTaggableInterface
 * @package common\components\taggable
 */
interface RFTaggableInterface
{
	/**
	 * @return ActiveQuery
	 */
	public function getTags(): ActiveQuery;

	/**
	 * @return int
	 */
	public function getTagType(): int ;
}