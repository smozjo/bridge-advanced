<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 7/25/14
 * Time: 17:17
 */

namespace common\components\grid;


use yii\grid\DataColumn;

class TreeColumn extends DataColumn
{
    public $attributeLevel;

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $value = parent::renderDataCellContent($model, $key, $index);

        return '<div class="col-md-' . (13 - $model->depth) . ' col-md-offset-' . ($model->depth - 1) . '">' . $value . '</div>';
    }

}
