<?php
/**
 * Created by PhpStorm.
 * User: prokhonenkov
 * Date: 15.03.19
 * Time: 16:48.
 */

namespace common\components\ckepage;

use yii\base\Behavior;
use yii\helpers\Json;
use yii\web\View;

class CkePageBehavior extends Behavior
{
    public $form;
    public $model;
    public $editorId = [];
    public $editableContentContainer = '#editableContent';
    public $plugin = null;

    public function events()
    {
        if (\Yii::$app->request->isAjax) {
            return [];
        }

        return [
            View::EVENT_END_PAGE => 'endPage',
            View::EVENT_BEGIN_PAGE => 'beginPage',
        ];
    }

    public function beginPage()
    {
        CkePageAsset::register($this->owner);
    }

    public function endPage()
    {
        $languages = Json::encode(array_map(function ($lang) {
            return strtolower($lang);
        }, array_keys(\Yii::$app->getModule('admin')->languages)));


        $this->owner->registerJs('new CkePage(
            "' . $this->form . '", 
            ' . $this->editorsToJsArray() . ', 
            ' . $languages . ',
            "' . $this->editableContentContainer . '",
            "' . $this->plugin . '"
        );');
    }

    protected function editorsToJsArray()
    {
        if (!is_array($this->editorId)) {
            $this->editorId = [$this->editorId];
        }
        $temp = array_map(fn($item) => '"' . addcslashes($item, "\0..\37\"\\") . '"', $this->editorId);
        return '[' . implode(',', $temp) . ']';
    }
}
