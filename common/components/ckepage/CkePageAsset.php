<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 05.04.19
 * Time 9:41
 */

namespace common\components\ckepage;


use yii\web\AssetBundle;

class CkePageAsset extends AssetBundle
{
	public $basePath = '@app';

	public $js = [
		'/js/ckeditor/CkePage.js',
	];
}