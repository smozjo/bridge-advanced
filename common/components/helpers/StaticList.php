<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 01.10.2019
 * Time 12:48
 */

namespace common\components\helpers;


class StaticList
{
	public static function getStaticTitle($attribute, $array, $translationCategory, int $id = null)
	{
		return $array[$id ?? \Yii::$app->request->get($attribute)]
			? \Yii::t($translationCategory, $array[$id ?? \Yii::$app->request->get($attribute)])
			: null;
	}

	public static function getList($array, $translationCategory)
	{
		return array_map(function ($type) use($translationCategory){
			return \Yii::t($translationCategory, $type);
		}, $array);
	}
}