<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 20.11.2019
 * Time 16:12
 */

namespace common\components\helpers;


use Imagine\Image\ManipulatorInterface;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

class FileHelper extends \yii\helpers\FileHelper
{
	public static function saveImageFromBase64(
		string $base64,
		string $savePath,
		string $fileName = null,
		string $prefix = null
	)
	{
		preg_match('/data:image\/(.*?);base64/', $base64, $ext);
		if(!$ext) {
			return false;
		}
		$data = str_replace('data:image/' . $ext[1] . ';base64,', '', $base64);
		$data = str_replace(' ', '+', $data);

		if(!$fileName) {
			$fileName = md5(microtime());
		}
		$basename = $prefix . $fileName . '.' . $ext[1];

		self::createDirectory($savePath);
		file_put_contents($savePath  . '/' . $basename, base64_decode($data));
		return [
			'filename' => $fileName,
			'basename' => $basename,
			'dirname' => $savePath,
			'extension' => $ext[1]
		];
	}

	public static function generateImageThumb($config, $path, $thumbPath)
	{
		$width = ArrayHelper::getValue($config, 'width');
		$height = ArrayHelper::getValue($config, 'height');
		$quality = ArrayHelper::getValue($config, 'quality', 100);
		$mode = ArrayHelper::getValue($config, 'mode', ManipulatorInterface::THUMBNAIL_OUTBOUND);
		$bg_color = ArrayHelper::getValue($config, 'bg_color', 'FFF');

		if (!$width || !$height) {
			$image = Image::getImagine()->open($path);
			$ratio = $image->getSize()->getWidth() / $image->getSize()->getHeight();
			if ($width) {
				$height = ceil($width / $ratio);
			} else {
				$width = ceil($height * $ratio);
			}
		}

		// Fix error "PHP GD Allowed memory size exhausted".
		ini_set('memory_limit', '512M');
		Image::$thumbnailBackgroundColor = $bg_color;
		Image::thumbnail($path, $width, $height, $mode)->save($thumbPath, ['quality' => $quality]);
	}

	public static function getThumbFileName($filename, $profile = 'thumb')
	{
		return $profile . '-' . $filename;
	}
}