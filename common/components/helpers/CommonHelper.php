<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 04.10.2019
 * Time 9:05
 */

namespace common\components\helpers;


class CommonHelper
{
	public static function  dateDifference($date_1 , $date_2 , $differenceFormat = null )
	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);

		$interval = date_diff($datetime1, $datetime2);

		if(!$differenceFormat) {
			return $interval;
		}
		return $interval->format($differenceFormat);
	}

	public static function getAge($date)
	{
		$age = self::dateDifference(
			$date,
			date('Y-m-d'),
			'%y'
		);

		return sprintf('%d %s',
			$age,
			\Yii::$app->i18n->format(
				'{n, plural, =0{лет} =1{год} one{год} few{года} many{лет} other{года}}',
				['n' => $age],
				\Yii::$app->language
			)
		);
	}
}