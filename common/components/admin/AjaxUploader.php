<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 29.01.2020
 * Time 15:58
 */

namespace common\components\admin;


use common\components\helpers\FileHelper;
use yii\base\Exception;

abstract class AjaxUploader
{
	abstract public function getDestination(): string ;

	abstract public function save(): bool ;

	abstract public function getFileName(): string ;

	public function removeDestination(): bool
	{
		$this->removeEmptyFolder($this->getDestination());

		if(is_dir($this->getDestination())) {
			return false;
		}

		return true;
	}

	public function getFileInfo()
	{
		return (object)pathinfo($this->getFilePath());
	}

	public function getFilePath()
	{
		return sprintf('%s/%s',
			$this->getDestination(),
			$this->getFileName()
		);
	}

	public function moveTo(string $filePath): bool
	{
		return rename($this->getFilePath(), $filePath);
	}

	public function remove(): bool
	{
		if(!is_file($this->getFilePath())) {
			return false;
		}
		return unlink($this->getFilePath());
	}

	public function isFileExist(): bool
	{
		try {
			return file_exists($this->getFilePath());
		} catch (\Exception $e) {
			return false;
		}
	}

	protected function removeEmptyFolder(string $dir): bool
	{
		if(!$this->dirIsEmpty($dir)) {
			return true;
		}

		FileHelper::removeDirectory($dir);

		if(is_dir($dir)) {
			return false;
		}

		return $this->removeEmptyFolder(dirname($dir));
	}

	private function dirIsEmpty($dir): bool
	{
		$handle = opendir($dir);
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				closedir($handle);
				return false;
			}
		}
		closedir($handle);
		return true;
	}
}