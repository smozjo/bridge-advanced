<?php
/**
 * Created by PhpStorm.
 * User: prokhonenkov
 * Date: 10.12.18
 * Time: 18:36
 */

namespace common\components\interfaces\translations;

use yii\db\ActiveQuery;

interface PageTranslationInterface extends MainTranslationInterface
{
	/**
	 * @return string
	 */
	public function getText(): ?string;

	/**
	 * @return string
	 */
	public function getSlug(): ?string;

}