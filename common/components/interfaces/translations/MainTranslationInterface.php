<?php
/**
 * Created by PhpStorm.
 * User: prokhonenkov
 * Date: 10.12.18
 * Time: 18:36
 */

namespace common\components\interfaces\translations;

use yii\db\ActiveQuery;

interface MainTranslationInterface
{
	/**
	 * @return string
	 */
	public function getTitle(): ?string;

	/**
	 * @return string
	 */
	public function getLang(): string;

	/**
	 * @return ActiveQuery
	 */
	public function getCurrentTranslation(): ?ActiveQuery;
}