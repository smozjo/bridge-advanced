<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.01.2020
 * Time 14:48
 */

namespace common\components\interfaces;


interface ParentClassInterface
{
	public function getMainModelclass(): string ;
}