<?php
return [
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'timeZone' => 'Asia/Almaty',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'on missingTranslation' => [
                        'common\helpers\TranslationHelper',
                        'missingTranslation'
                    ],
                    'enableCaching' => true,
                    'cachingDuration' => 3600*24,
                ],
            ],
        ],
    ],
];
