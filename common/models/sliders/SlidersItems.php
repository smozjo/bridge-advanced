<?php

namespace common\models\sliders;

use common\components\behaviors\AutoFillTranslationForm;
use common\components\traits\FakeAttributeTrait;
use common\core\CoreActiveRecord;
use common\models\sliders\query\SlidersItemsQuery;
use common\models\sliders\translation\SlidersItemsTranslation;
use common\models\User;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sliders_items".
 *
 * @property int $id
 * @property int $slider_id
 * @property string $thumb_desktop
 * @property string $thumb_adaptive
 * @property int $user_id
 * @property int $position
 * @property int $is_active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Sliders $slider
 * @property User $user
 * @property SlidersItemsTranslation[] $slidersItemsTranslations
 * @property SlidersItemsTranslation $currentTranslation
 */
class SlidersItems extends CoreActiveRecord
{
	use FakeAttributeTrait;

	public $fakeAttributes = [
		'thumb_desktop',
		'thumb_adaptive'
	];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sliders_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slider_id', 'user_id', 'position', 'is_active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['thumb_desktop', 'thumb_adaptive'], 'image'],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sliders::class, 'targetAttribute' => ['slider_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
		return ArrayHelper::merge(parent::attributeLabels(), [
			'thumb_desktop' => \Yii::t('app', 'Обложка десктоп'),
			'thumb_adaptive' => \Yii::t('app', 'Обложка адаптив'),
		]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(Sliders::class, ['id' => 'slider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlidersItemsTranslations()
    {
        return $this->hasMany(SlidersItemsTranslation::class, ['item_id' => 'id']);
    }

	public function getCurrentTranslation(): ActiveQuery
	{
		return $this->hasOne(SlidersItemsTranslation::class, ['item_id' => 'id'])
			->onCondition([SlidersItemsTranslation::tableName() . '.lang' => \Yii::$app->language]);;
	}

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'translation' => self::behaviorsTranslation(
				SlidersItemsTranslation::class,
				'item_id'
			),
			self::behaviorsUserId(),
			self::behaviorsTimestamp(),
			'thumb_desktop' => self::uploadBehavior('thumb_desktop'),
			'thumb_adaptive' => self::uploadBehavior('thumb_adaptive'),
			parent::behaviorsPositionSort('position', ['slider_id']),
			'autoFillTranslationForm' => [
				'class' => AutoFillTranslationForm::class,
				'excludeAttributes' => ['item_id']
			]
		]);
	}

	private static function uploadBehavior($attribute, $instanceByName = false)
	{
		if(strpos($attribute, 'thumb_desktop') !== false) {
			$thumb = [
				'basic' => ['width' => 1920, 'height' => 880, 'quality' => 90, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
				'preview' => ['width' => 300, 'height' => 172, 'quality' => 90, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND]
			];
		} elseif(strpos($attribute, 'thumb_adaptive') !== false) {
			$thumb = [
				'basic' => ['width' => 386, 'height' => 440, 'quality' => 90, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
				'preview' => ['width' => 300, 'height' => 172, 'quality' => 90, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND]
			];
		}

		return self::behaviorsImageUpload(
			$attribute,
			'@webroot/media/sliders_items/{id}',
			'@web/media/sliders_items/{id}',
			$thumb,
			false,
			$instanceByName
		);
	}

	public static function multiSave($sliderId)
	{
		$bodyParams = \Yii::$app->request->getBodyParams();


		foreach (\Yii::$app->request->post('SlidersItemsRepeater') as $index => $item) {

			\Yii::$app->request->setBodyParams($item);

			$series = null;
			if(isset($item['SlidersItems']['id'])) {
				$series = self::findOne($item['SlidersItems']['id']);
			}

			if(!$series) {
				$series = new self();
			}
			$series->slider_id = $sliderId;
			$series->load($item);

			$series->detachBehavior('thumb_desktop');
			$series->detachBehavior('thumb_adaptive');
			$series->attachBehavior(
				'thumb_adaptive-' . $index,
				self::uploadBehavior($series->formName() . "[$index][thumb_adaptive]", true)
			);
			$series->attachBehavior(
				'thumb_desktop-' . $index,
				self::uploadBehavior($series->formName() . "[$index][thumb_desktop]", true)
			);
			$series->save();
		}
		\Yii::$app->request->setBodyParams($bodyParams);
	}

	public static function find()
	{
		return new SlidersItemsQuery(get_called_class());
	}

	public function getMainImageDesktop()
	{
		return $this->getThumbUploadUrl('thumb_desktop', 'basic');
	}

	public function getMainImageAdaptive()
	{
		return $this->getThumbUploadUrl('thumb_adaptive', 'basic');
	}

	public function getPreviewsImageDesktop()
	{
		return $this->getThumbUploadUrl('thumb_desktop', 'preview');
	}

	public function getPreviewImageAdaptive()
	{
		return $this->getThumbUploadUrl('thumb_adaptive', 'preview');
	}

    /**
     * Абсолютная ссылка на изображения для header
     *
     * @return string
     */
    public function getImage()
    {
        return Yii::$app->urlManagerAdmin->createUrl(['/media/sliders_items/' . $this->id . '/' . $this->thumb_desktop]);
    }
}
