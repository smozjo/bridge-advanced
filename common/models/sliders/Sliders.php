<?php

namespace common\models\sliders;

use common\components\behaviors\AutoFillTranslationForm;
use common\core\CoreActiveRecord;
use common\models\handbooks\Sections;
use common\models\sliders\query\SlidersQuery;
use common\models\sliders\translation\SlidersTranslation;
use common\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sliders".
 *
 * @property int $id
 * @property int $section_id
 * @property int $user_id
 * @property int $is_active
 * @property int $position
 * @property int $section_item_id
 * @property string $finished_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Sections $section
 * @property User $user
 * @property SlidersItems[] $slidersItems
 * @property SlidersItems[] $activeSlidersItems
 * @property SlidersTranslation[] $slidersTranslations
 */
class Sliders extends CoreActiveRecord
{
	public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sliders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['section_id'], 'required'],
            [['section_id', 'section_item_id', 'user_id', 'is_active', 'position'], 'integer'],
            [['finished_at', 'created_at', 'updated_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
	{
		return ArrayHelper::merge(parent::attributeLabels(), [
			'section_id' => Yii::t('app', 'Раздел'),
			'section_item_id' => Yii::t('app', 'Подраздел'),
		]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSection()
	{
		return $this->hasOne(Sections::class, ['id' => 'section_id'])
			->currentTranslation();
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlidersItems()
    {
        return $this->hasMany(SlidersItems::class, ['slider_id' => 'id'])
			->currentTranslation();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveSlidersItems()
    {
        return $this->hasMany(SlidersItems::class, ['slider_id' => 'id'])->where(['is_active' => 1])
            ->currentTranslation();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlidersTranslations()
    {
        return $this->hasMany(SlidersTranslation::class, ['slider_id' => 'id']);
    }

	public function getCurrentTranslation(): ActiveQuery
	{
		return $this->hasOne(SlidersTranslation::class, ['slider_id' => 'id'])
			->onCondition([SlidersTranslation::tableName() . '.lang' => \Yii::$app->language]);
	}

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'translation' => self::behaviorsTranslation(
				SlidersTranslation::class,
				'slider_id'
			),
			self::behaviorsUserId(),
			self::behaviorsTimestamp(),
            'softDeleteBehavior' => [
                'class' => 'yii2tech\ar\softdelete\SoftDeleteBehavior',
                'softDeleteAttributeValues' => ['is_deleted' => 1],
                'invokeDeleteEvents' => false,
            ],
			parent::behaviorsPositionSort('position', ['section_id']),
			'autoFillTranslationForm' => [
				'class' => AutoFillTranslationForm::class,
				'excludeAttributes' => ['slider_id']
			]
		]);
	}


	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
		if(\Yii::$app->request->post('SlidersItemsRepeater')) {
			SlidersItems::multiSave($this->id);
		}
	}

	public static function find()
	{
		return new SlidersQuery(get_called_class());
	}

	public function extraFields()
    {
        return ['slidersItems'];
    }
}
