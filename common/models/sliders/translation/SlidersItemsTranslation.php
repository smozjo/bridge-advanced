<?php

namespace common\models\sliders\translation;

use common\core\CoreActiveRecord;
use common\models\sliders\SlidersItems;
use Yii;

/**
 * This is the model class for table "sliders_items_translation".
 *
 * @property int $item_id
 * @property string $lang
 * @property string $title
 * @property string $slug
 * @property string $description
 *
 * @property SlidersItems $item
 */
class SlidersItemsTranslation extends CoreActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sliders_items_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lang'], 'required'],
            [['item_id'], 'integer'],
            [['lang'], 'string', 'max' => 20],
            [['title', 'slug'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['item_id', 'lang'], 'unique', 'targetAttribute' => ['item_id', 'lang']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => SlidersItems::class, 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(SlidersItems::class, ['id' => 'item_id']);
    }

	public function behaviors()
	{
		return [
			'slug' => self::behaviorsSlug('title')
		];
	}
}
