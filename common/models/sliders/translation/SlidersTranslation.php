<?php

namespace common\models\sliders\translation;

use common\core\CoreActiveRecord;
use common\models\sliders\Sliders;
use Yii;

/**
 * This is the model class for table "sliders_translation".
 *
 * @property int $slider_id
 * @property string $lang
 * @property string $title
 * @property string $slug
 *
 * @property Sliders $slider
 */
class SlidersTranslation extends CoreActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sliders_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lang'], 'required'],
            [['slider_id'], 'integer'],
            [['lang'], 'string', 'max' => 20],
            [['title', 'slug'], 'string', 'max' => 255],
            [['slider_id', 'lang'], 'unique', 'targetAttribute' => ['slider_id', 'lang']],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sliders::class, 'targetAttribute' => ['slider_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(Sliders::class, ['id' => 'slider_id']);
    }

	public function behaviors()
	{
		return [
			'slug' => self::behaviorsSlug('title')
		];
	}
}
