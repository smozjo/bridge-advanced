<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 07.10.2019
 * Time 16:59
 */

namespace common\models\sliders\query;


use common\core\CacheActiveQuery;

class SlidersQuery extends CacheActiveQuery
{
	public function withSection()
	{
		return $this->innerJoinWith('section');
	}

	public function withSlides()
	{
		return $this->innerJoinWith('slides');
	}
}