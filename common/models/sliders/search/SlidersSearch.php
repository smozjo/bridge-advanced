<?php

namespace common\models\sliders\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\sliders\Sliders;

/**
 * SlidersSearch represents the model behind the search form of `common\models\sliders\Sliders`.
 */
class SlidersSearch extends Sliders
{
	public $searchTitle;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'section_id', 'user_id', 'is_deleted', 'is_active'], 'integer'],
            [['finished_at', 'created_at', 'updated_at', 'searchTitle'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->is_deleted = 0;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {
        $query = Sliders::find()
		->currentTranslation()
		->withSection();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => [
				'defaultOrder' => ['position' => SORT_ASC]
			]
        ]);

        $this->load($params, $formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'updated_at' => $this->updated_at,
        ]);

		if($this->finished_at) {
			$query->andWhere([
				'LIKE',
				'finished_at',
				date('Y-m-d H:i', strtotime($this->finished_at))
			]);
		}
		if($this->created_at) {
			$query->andWhere([
				'LIKE',
				'created_at',
				date('Y-m-d H:i', strtotime($this->created_at))
			]);
		}

        $query->andFilterWhere(['section_id' => $this->section_id])
            ->andFilterWhere(['sliders.is_active' => $this->is_active])
            ->andFilterWhere(['title' => $this->searchTitle]);

        $query->andWhere(['sliders.is_deleted' => $this->is_deleted]);

        return $dataProvider;
    }
}
