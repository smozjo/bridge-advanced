<?php

namespace common\models\sliders\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\sliders\SlidersItems;

/**
 * SlidersSearchItems represents the model behind the search form of `common\models\sliders\SlidersItems`.
 */
class SlidersSearchItems extends SlidersItems{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'slider_id', 'user_id', 'position', 'is_active'], 'integer'],
            [['thumb_desktop', 'thumb_adaptive', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SlidersItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'slider_id' => $this->slider_id,
            'user_id' => $this->user_id,
            'position' => $this->position,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'thumb_desktop', $this->thumb_desktop])
            ->andFilterWhere(['like', 'thumb_adaptive', $this->thumb_adaptive])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

        return $dataProvider;
    }
}
