<?php

namespace common\models\menu\translation;

use common\core\CoreActiveRecord;
use common\models\menu\Menu;
use Yii;

/**
 * This is the model class for table "menu_translation".
 *
 * @property integer $menu_id
 * @property string $lang
 * @property string $title
 *
 * @property Menu $menu
 */
class MenuTranslation extends CoreActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'title'], 'required'],
            [['menu_id'], 'integer'],
            [['lang', 'title'], 'string', 'max' => 255],
            [['menu_id', 'lang'], 'unique', 'targetAttribute' => ['menu_id', 'lang'], 'message' => 'The combination of Menu ID and Lang has already been taken.'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::class, 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => 'Menu ID',
            'lang' => 'Lang',
            'title' => 'Название меню',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::class, ['id' => 'menu_id']);
    }

}
