<?php

namespace common\models\menu\translation;

use common\core\CoreActiveRecord;
use common\models\menu\MenuItem;
use Yii;

/**
 * This is the model class for table "menu_item_translation".
 *
 * @property integer $menu_item_id
 * @property string $lang
 * @property string $title
 * @property string $link
 * @property string $params
 *
 * @property MenuItem $menuItem
 */
class MenuItemTranslation extends CoreActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_item_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'title', 'link'], 'required'],
            [['menu_item_id'], 'integer'],
            [['lang', 'title', 'link', 'params'], 'string', 'max' => 255],
            [['menu_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuItem::class, 'targetAttribute' => ['menu_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_item_id' => 'Menu Item ID',
            'lang' => 'Lang',
            'title' => 'Название',
            'link' => 'Ссылка',
            'params' => 'Параметры',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::class, ['id' => 'menu_item_id']);
    }

}
