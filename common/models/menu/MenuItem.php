<?php

namespace common\models\menu;

use common\components\traits\NestedSetTree;
use common\core\CoreActiveRecord;
use common\models\menu\query\MenuItemQuery;
use common\models\menu\translation\MenuItemTranslation;
use Bridge\Core\Behaviors\TranslationBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\base\InvalidParamException;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\components\helpers\StringHelper;

/**
 * This is the model class for table "menu_item".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $type
 * @property integer $is_new_window
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property integer $parent_id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $nofollow
 * @property string $image
 *
 * @property Menu $menu
 * @property MenuItemTranslation[] $menuItemTranslations
 *
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false)
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb')
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 */
class MenuItem extends CoreActiveRecord implements MenuItemTranslationInterface
{
    use NestedSetTree;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'is_new_window', 'is_active', 'parent_id', 'root', 'lft', 'rgt', 'depth', 'nofollow'], 'integer'],
			[['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg', 'svg']],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::class, 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Меню',
            'title' => 'Название',
            'type' => 'Тип',
            'is_new_window' => 'В новом окне',
            'is_active' => 'Активнсть',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'parent_id' => 'Родитель',
            'root' => 'Root',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'depth',
            'nofollow' => 'Запретить индексировать',
            'image' => 'Картинка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::class, ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemTranslations()
    {
        return $this->hasMany(MenuItemTranslation::class, ['menu_item_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getParent()
	{
		return $this->hasOne(MenuItem::class, ['id' => 'parent_id']);
	}


    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'translationModelClass' => MenuItemTranslation::class,
                'translationModelRelationColumn' => 'menu_item_id'
            ],
            [
                'class' => NestedSetsBehavior::class,
                'treeAttribute' => 'root',
                'depthAttribute' => 'depth'
            ],
			'imageUpload' => self::behaviorsImageUpload(
				'image',
				'@webroot/media/menu_item/{id}',
				'@web/media/menu_item/{id}',
				[]

			)
        ];
    }

    public static function find()
	{
		return new MenuItemQuery(get_called_class());
	}

	public function beforeValidate()
    {
    	$this->parent_id = (int)$this->parent_id;
        return parent::beforeValidate();
    }

	public function getCurrentTranslation(): ActiveQuery
	{
		return $this->hasOne(MenuItemTranslation::class, ['menu_item_id' => 'id'])
			->onCondition([MenuItemTranslation::tableName() . '.lang' => \Yii::$app->language]);
	}

	public function getUrl()
	{
		if ($this->type != 'external') {
			try {
				$params = Json::decode($this->checkJsonParams($this->params));
			} catch (InvalidParamException $e) {
				$params = null;
			}

			if ($params !== null) {
				$url = ArrayHelper::merge(['/' . $this->link], $params);
			} else {
				$url = ['/' . $this->link];
			}

			return $url;
		}

		return $this->link;
	}

	public function checkJsonParams()
	{
		return StringHelper::mb_str_replace("'", '"', $this->getParams());
	}

	public function getParams(): ?string
	{
		return $this->currentTranslation->params;
	}

	public function getLink(): string
	{
		return $this->currentTranslation->link;
	}

	public function getImage(): string
	{
		return $this->getUploadUrl('image');
	}

	public function getImagePath(): string
	{
		return $this->getUploadPath('image');
	}

	public static function getDropDownData()
	{
		return self::getDropdownList('title', 'id', [
			'join' => ['currentTranslation'],
			'condition' => ['menu_id' => \Yii::$app->request->post('menu_id')]
		]);
	}
}
