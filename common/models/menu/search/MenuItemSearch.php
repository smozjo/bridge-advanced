<?php

namespace common\models\menu\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\menu\MenuItem;

/**
 * MenuItemSearch represents the model behind the search form of `common\models\menu\MenuItem`.
 */
class MenuItemSearch extends MenuItem{
    public $title;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'menu_id', 'is_new_window', 'is_active', 'parent_id', 'root', 'lft', 'rgt', 'depth', 'nofollow'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MenuItem::find()
        ->innerJoinWith('currentTranslation')
        ->innerJoinWith('menu.currentTranslation');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['root' => SORT_ASC, 'lft' => SORT_ASC]],
            'pagination' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'menu_item.menu_id', $this->menu_id])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'is_new_window', $this->is_new_window])
            ->andFilterWhere(['like', 'is_active', $this->is_active])
            ->andFilterWhere(['like', 'root', $this->root])
            ->andFilterWhere(['like', 'lft', $this->lft])
            ->andFilterWhere(['like', 'rgt', $this->rgt])
            ->andFilterWhere(['like', 'depth', $this->depth])
            ->andFilterWhere(['like', 'nofollow', $this->nofollow]);

        return $dataProvider;
    }
}
