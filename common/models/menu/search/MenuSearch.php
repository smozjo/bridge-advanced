<?php

namespace common\models\menu\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\menu\Menu;
use yii\helpers\ArrayHelper;

/**
 * MenuSearch represents the model behind the search form of `common\models\menu\Menu`.
 */
class MenuSearch extends Menu{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'is_deleted'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->is_deleted = 0;
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {
        $query = Menu::find()
			->innerJoinWith('currentTranslation');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, $formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['is_active' => $this->is_active]);

        $query->andWhere(['menu.is_deleted' => $this->is_deleted]);

        return $dataProvider;
    }
}
