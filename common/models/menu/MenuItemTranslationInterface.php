<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 03.04.19
 * Time 11:04
 */

namespace common\models\menu;


use common\components\interfaces\translations\MainTranslationInterface;

interface MenuItemTranslationInterface extends MainTranslationInterface
{
	public function getParams(): ?string ;

	public function getLink(): string ;
}