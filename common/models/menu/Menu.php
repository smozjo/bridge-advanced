<?php

namespace common\models\menu;

use common\components\interfaces\translations\MainTranslationInterface;
use common\core\CoreActiveRecord;

use admin\modules\menu\components\MenuInterface;
use common\models\menu\query\MenuQuery;
use common\models\menu\translation\MenuTranslation;
use Bridge\Core\Behaviors\TranslationBehavior;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $is_active
 *
 * @property MenuItem[] $menuItems
 * @property MenuTranslation[] $menuTranslations
 */
class Menu extends CoreActiveRecord implements MainTranslationInterface, MenuInterface
{
    const MAIN = 'mainpage';
    const PAGE = 'page';
    const EXTERNAL = 'external';


    public static $linksTitles = [
		self::MAIN => 'Главная страница',
        self::PAGE => 'Текстовые страницы',
        self::EXTERNAL => 'Внешние ссылки',
    ];

    public static $links = [
        self::MAIN => [
            'route' => 'mainpage/index'
        ],
		self::PAGE => [
            'route' => 'pages/view',
			'params' => '{"slug":"введите слаг"}'
        ],
    ];

    const MENU_MAIN = 3;
    const MENU_FOOTER = 4;
    const MENU_SOCIAL = 5;
    const MENU_APPS = 6;
    const MENU_MOBILE_FOOTER = 7;
    const MENU_MOBILE_APP_MAIN = 8;
    const MENU_MOBILE_APP_SECOND = 9;
    const MENU_PROFILE_HEADER = 10;
    const MENU_FILTER_MOVIES = 11;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => \Yii::t('app', 'Активность'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(MenuItem::class, ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuTranslations()
    {
        return $this->hasMany(MenuTranslation::class, ['menu_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'translationModelClass' => MenuTranslation::class,
                'translationModelRelationColumn' => 'menu_id'
            ],
            'softDeleteBehavior' => [
                'class' => 'yii2tech\ar\softdelete\SoftDeleteBehavior',
                'softDeleteAttributeValues' => ['is_deleted' => 1],
                'invokeDeleteEvents' => false,
            ],
        ];
    }

	public function getCurrentTranslation(): ActiveQuery
	{
		return $this->hasOne(MenuTranslation::class, ['menu_id' => 'id'])
			->onCondition([MenuTranslation::tableName() . '.lang' => \Yii::$app->language]);
	}

	public static function find()
	{
		return new MenuQuery(get_called_class());
	}

	public function getItems(): array
	{
		$data = array();
		foreach ($this->menuItems as $item) {
			$node = [
				'label' => $item->title,
				'url' => $item->getUrl(),
				'isNewWindow' => $item->is_new_window,
				'isNofollow' => $item->nofollow,
				'image' => $item->image
					? \Yii::getAlias(sprintf('@web/media/%s/%d/',
						MenuItem::tableName(),
						$item->id
					)) . $item->image
					: null
			];

			if ($item->depth == 0) {
				$data[$item->id] = $node;
			} elseif ($item->depth > 0) {
				if (!isset($data[$item->parent_id])) {
					$data[$item->parent_id] = [];
				}
				$data[$item->parent_id]['items'][] = $node;
			} else {
				$data[$item->id] = $node;
			}
		}

		return $data;
	}

	public function extraFields()
    {
        return ['items'];
    }
}
