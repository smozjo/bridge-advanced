<?php

namespace common\models\setting;

class Setting extends \Bridge\Core\Models\Settings
{
    public function extraFields()
    {
        return [
            'translation'
        ];
    }

}