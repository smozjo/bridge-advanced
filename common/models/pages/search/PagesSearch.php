<?php

namespace common\models\pages\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\pages\Pages;

/**
 * PagesSearch represents the model behind the search form of `common\models\pages\Pages`.
 */
class PagesSearch extends Pages
{
	public $searchTitle;
	public $searchSubTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'is_deleted', 'position'], 'integer'],
            [['created_at', 'updated_at', 'searchTitle', 'searchSubTitle'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->is_deleted = 0;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
		->joinWith('currentTranslation');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'position' => $this->position,
            'created_at' => $this->created_at,
        ]);

        $query
			->andFilterWhere(['like', 'pages_translation.title', $this->searchTitle,])
			->andFilterWhere(['like', 'pages_translation.subtitle', $this->searchSubTitle,])
			->andFilterWhere(['like', 'updated_at', $this->updated_at])
			->andFilterWhere(['like', 'is_active', $this->is_active]);

        $query->andWhere(['pages.is_deleted' => $this->is_deleted]);

        return $dataProvider;
    }
}
