<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 03.04.19
 * Time 15:24
 */

namespace common\models\pages\query;


use common\core\CacheActiveQuery;
use yii\db\ActiveQuery;

class PagesQuery extends CacheActiveQuery
{
	public function withTranslations(string $alias = null)
	{
		return $this->joinWith(['translations' => function(ActiveQuery $query) use($alias){
			if($alias) {
				$query->alias($alias);
			}
		}]);
	}
}