<?php

namespace common\models\pages;

use app\modules\gallery\models\GalleryAbstract;
use Yii;

/**
 * This is the model class for table "pages_gallery".
 *
 * @property int $id
 * @property int $page_id
 * @property string $path
 * @property int $position
 *
 * @property Pages $page
 */
class PagesGallery extends GalleryAbstract
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'position'], 'integer'],
            [['path'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pages::class, 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'path' => 'Path',
            'position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Pages::class, ['id' => 'page_id']);
    }

	public static function getRelationIdFieldName()
	{
		return 'page_id';
	}

	public static function getThumbnailsConfig(): array
	{
		return [];
	}
}
