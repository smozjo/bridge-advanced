<?php

namespace common\models\pages\translation;

use common\core\CoreActiveRecord;
use common\models\pages\Pages;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pages_translation".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $lang
 * @property string $title
 * @property string $image
 * @property string $text
 * @property string $additionl_fields
 * @property string $seo_text
 *
 * @property Pages $page
 *
 * @method string getThumbUploadPath($attribute, $profile = 'thumb', $old = false) 
 * @method string|null getThumbUploadUrl($attribute, $profile = 'thumb') 
 * @method string|null getUploadPath($attribute, $old = false) Returns file path for the attribute.
 * @method string|null getUploadUrl($attribute) Returns file url for the attribute.
 * @method bool sanitize($filename) Replaces characters in strings that are illegal/unsafe for filename.
 * @method mixed evaluateAttributes($event) Evaluates the attribute value and assigns it to the current attributes.
 */
class PagesTranslation extends CoreActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang'], 'required'],
            [['page_id'], 'integer'],
            [['text', 'additionl_fields', 'seo_text'], 'string'],
            [['lang'], 'string', 'max' => 20],
            [['title', 'subtitle', 'slug'], 'string', 'max' => 255],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg']],
            [['lang', 'page_id'], 'unique', 'targetAttribute' => ['lang', 'page_id'], 'message' => 'The combination of Page ID and Lang has already been taken.'],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pages::class, 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Pages::class, ['id' => 'page_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'slug' => self::behaviorsSlug('title', 'slug', false),
			'imageUpload' => self::behaviorsImageUpload(
				'image',
				'@webroot/media/pages_translation/{id}',
				'@web/media/pages_translation/{id}',
				[
					'thumb' => ['width' => 200, 'height' => 200, 'quality' => 90],
					'preview' => ['width' => 219, 'height' => 163, 'quality' => 90]
				],
				true,
				true
			),
        ];
    }

	public function beforeValidate()
	{
		$this->additionl_fields = json_encode((array)$this->additionl_fields);

		return parent::beforeValidate();
	}

	public function afterFind()
	{
		$this->additionl_fields = json_decode($this->additionl_fields, true);
		parent::afterFind();
	}
}
