<?php

namespace common\models\pages;

use common\components\behaviors\AutoFillTranslationForm;
use common\components\interfaces\ParentClassInterface;
use common\components\interfaces\translations\PageTranslationInterface;
use common\core\CoreActiveRecord;
use app\modules\before_after\models\BeforeAfter;
use app\modules\contacts\models\Contacts;
use app\modules\faq\models\Faq;
use common\models\pages\query\PagesQuery;
use common\models\pages\translation\PagesTranslation;
use app\modules\professionals\models\Professionals;
use app\modules\services\models\Services;
use Bridge\Core\Models\MetaModel;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property integer $is_active
 * @property integer $position
 * @property string $image
 * @property string $text
 * @property string $title
 * @property string $additionl_fields
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PagesTranslation[] $translations
 * @property PagesTranslation $currentTranslation
 *
 * @method bool movePrev() Moves owner record by one position towards the start of the list.
 * @method bool moveNext() Moves owner record by one position towards the end of the list.
 * @method bool moveFirst() Moves owner record to the start of the list.
 * @method bool moveLast() Moves owner record to the end of the list.
 * @method bool moveToPosition($position) Moves owner record to the specific position.
 * @method bool getIsFirst() Checks whether this record is the first in the list.
 * @method bool getIsLast() Checks whether this record is the the last in the list.
 * @method \BaseActiveRecord|static|null findPrev() Finds record previous to this one.
 * @method \BaseActiveRecord|static|null findNext() Finds record next to this one.
 * @method \BaseActiveRecord|static|null findFirst() Finds the first record in the list.
 * @method \BaseActiveRecord|static|null findLast() Finds the last record in the list.
 * @method mixed beforeInsert($event) Handles owner 'beforeInsert' owner event, preparing its positioning.
 * @method mixed beforeUpdate($event) Handles owner 'beforeInsert' owner event, preparing its possible re-positioning.
 */
class Pages extends CoreActiveRecord implements PageTranslationInterface, ParentClassInterface
{
	public static $systemPages = [

	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'position'], 'integer'],
            [['additionl_fields'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
			[['image'], 'image'],
        ];
    }

    public function attributeLabels()
	{
		return ArrayHelper::merge(parent::attributeLabels(), [
			'searchSubTitle' => 'Подзаголовок'
		]);
	}

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PagesTranslation::class, ['page_id' => 'id']);
    }

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'translation' => self::behaviorsTranslation(PagesTranslation::class, 'page_id'),
			'timestamp' => self::behaviorsTimestamp(),
			'metaTag' => self::behaviorsMetaTag(
				'translation.title',
				'translation.short_text',
				self::class
			),
            'positionSort' => self::behaviorsPositionSort(),
			'autoFillTranslationForm' => [
				'class' => AutoFillTranslationForm::class,
				'excludeAttributes' => ['movie_id']
			],
            'image' => self::behaviorsImageUpload(
                'image',
                '@webroot/media/pages/{id}',
                '@web/media/pages/{id}',
                [
                    '900x600' => ['width' => 900, 'height' => 600, 'quality' => 90, 'mode' => ManipulatorInterface::THUMBNAIL_OUTBOUND],
                ]
            ),
            'softDeleteBehavior' => [
                'class' => 'yii2tech\ar\softdelete\SoftDeleteBehavior',
                'softDeleteAttributeValues' => ['is_deleted' => 1],
                'invokeDeleteEvents' => false,
            ],
        ];
    }

    public function getMeta()
	{
		return $this->hasOne(MetaModel::class, ['model_id' => 'id'])
			->onCondition([
				'meta_models.model' => self::class,
			])
			->joinWith(['metaTag.metaTagTranslations' => function(ActiveQuery $query) {
				$query->onCondition(['meta_tag_translations.lang' => Yii::$app->language]);
			}]);
	}

	public function getCurrentTranslation(): ActiveQuery
	{
		return $this->hasOne(PagesTranslation::class, ['page_id' => 'id'])
			->onCondition([PagesTranslation::tableName() . '.lang' => \Yii::$app->language]);
	}

	public function getText(): string
	{
		return $this->currentTranslation->text;
	}

	public function getSubTitle(): ?string
	{
		return $this->currentTranslation->subtitle;
	}

	public function getSeoText(): string
	{
		return $this->currentTranslation->seo_text;
	}


	public function beforeValidate()
	{
		$this->additionl_fields = json_encode((array)$this->additionl_fields);

		return parent::beforeValidate();
	}

	public function afterFind()
	{
		$this->additionl_fields = json_decode($this->additionl_fields, true);
		parent::afterFind();
	}

	public static function find()
	{
		return new PagesQuery(get_called_class());
	}

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
	}

	public function getImage()
	{
		return $this->getUploadUrl('image');
	}

    public function getImageUrl()
    {
        return \Yii::$app->urlManagerAdmin->createUrl(['/media/pages/' . $this->id . '/' . $this->image]);
    }

	public function getMetaTitle()
	{
		return $this->meta->metaTag->metaTagTranslations[0]
			? $this->meta->metaTag->metaTagTranslations[0]->title
			: null;
	}

	public function getMetaDescription()
	{
		return $this->meta->metaTag->metaTagTranslations[0]
			? $this->meta->metaTag->metaTagTranslations[0]->description
			: null;
	}

	public function getMetaImage()
	{
		return $this->meta->metaTag->metaTagTranslations
			? $this->meta->metaTag->metaTagTranslations[0]->getUploadUrl('image')
			: null;
	}

	public function getMainModelclass(): string
	{
		return self::class;
	}

    public function getMainImage()
    {
        return \Yii::$app->urlManagerAdmin->createUrl(['/media/pages/' . $this->id . '/' . $this->image]);
    }
}
