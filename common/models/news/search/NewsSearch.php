<?php

namespace common\models\news\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\news\News;
use yii\helpers\ArrayHelper;

/**
 * NewsSearch represents the model behind the search form of `common\models\news\News`.
 */
class NewsSearch extends News
{

	public const ON_WORK = 1;
	public const WAIT_MODERATION = 2;
	public const IS_PUBLISH = 3;
	public const IN_ACTIVE = 4;

	public static $workStatuses = [
		self::IN_ACTIVE => 'Неактивный',
		self::ON_WORK => 'В работе',
		self::WAIT_MODERATION => 'Ожидает модерации',
		self::IS_PUBLISH => 'Опубликован'
	];
	public $isModeration;
    public $searchTitle;
    public $searchShortText;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'is_deleted', 'isModeration'], 'integer'],
            [['publish_at', 'searchTitle','searchShortText'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->is_deleted = 0;
    }

    public function attributeLabels()
	{
		return ArrayHelper::merge(parent::attributeLabels(), [
			'isModeration' => 'Модерация'
		]);
	}

	/**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->currentTranslation();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'publish_at' => SORT_DESC,
				]
             ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		if((int)$this->isModeration === self::ON_WORK ) {
			$query->andWhere([
        		'publish_at' => null,
				'is_active' => self::STATE_INACTIVE
			]);
		} elseif((int)$this->isModeration === self::WAIT_MODERATION ) {
			$query->andWhere([
				'publish_at' => null,
				'is_active' => self::STATE_ACTIVE
			]);
		} elseif((int)$this->isModeration === self::IS_PUBLISH ) {
			$query->andWhere(['AND',
				['IS NOT', 'publish_at', null,],
				['is_active' => self::STATE_ACTIVE]
			]);
		} elseif((int)$this->isModeration === self::IN_ACTIVE ) {
			$query->andWhere(['AND',
				['IS NOT', 'publish_at', null,],
				['is_active' => self::STATE_INACTIVE]
			]);
		}

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'publish_at' => $this->publish_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query
            ->andFilterWhere(['like', 'news_translation.title', $this->searchTitle,])
            ->andFilterWhere(['like', 'news_translation.short_text', $this->searchShortText,])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

        $query->andWhere(['news.is_deleted' => $this->is_deleted]);
        return $dataProvider;
    }
}
