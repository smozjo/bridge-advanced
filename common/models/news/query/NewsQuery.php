<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 11.02.2020
 * Time 14:19
 */

namespace common\models\news\query;


use common\core\CacheActiveQuery;
use common\core\CoreActiveQuery;
use yii\db\ActiveQuery;

class NewsQuery extends CacheActiveQuery
{

    public function currentTranslation($eagerLoading = true): CoreActiveQuery
    {
        return $this->innerJoinWith(['currentTranslation' => function(ActiveQuery $query){
            $query->andOnCondition([
                'AND', ['<>', 'slug', ''], ['IS NOT', 'slug', null]
            ]);
        }], $eagerLoading);
    }

	public function withTranslations(string $alias = null)
	{
		return $this->joinWith(['translations' => function(ActiveQuery $query) use($alias){
			if($alias) {
				$query->alias($alias);
			}
		}]);
	}

	public function notPublish()
	{
		return $this->andWhere([
			'publish_at' => null,
		]);
	}

}