<?php

namespace common\models\news\translation;

use common\core\CoreActiveRecord;
use common\models\news\News;

/**
 * This is the model class for table "news_translation".
 *
 * @property int $id
 * @property int $news_id
 * @property string $lang
 * @property string $title
 * @property string $slug
 * @property string $short_text
 * @property string $text
 *
 * @property News $news
 */
class NewsTranslation extends CoreActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lang', 'title'], 'required'],
            [['news_id'], 'integer'],
            [['text'], 'string'],
            [['short_text'], 'string', 'max' => 1000],
            [['lang'], 'string', 'max' => 20],
            [['title', 'slug'], 'string', 'max' => 255],
            [['news_id', 'lang'], 'unique', 'targetAttribute' => ['news_id', 'lang']],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::class, 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::class, ['id' => 'news_id']);
    }

	public function behaviors()
	{
		return [
			'slug' => self::behaviorsSlug('title', 'slug', true),
		];
	}

}
