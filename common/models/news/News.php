<?php

namespace common\models\news;

use common\components\cropper\CropperBehavior;
use common\components\interfaces\ParentClassInterface;
use common\core\CoreActiveRecord;
use common\models\news\translation\NewsTranslation;
use common\models\news\query\NewsQuery;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $is_active
 * @property string $image
 * @property string publish_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property NewsTranslation[] $newsTranslations
 * @property NewsTranslation $currentTranslation
 */
class News extends CoreActiveRecord implements ParentClassInterface
{
    public $cropper650x400;
    public $cropper422x200;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['publish_at', 'required'],
            [['is_active'], 'integer'],
            [['publish_at', 'created_at', 'updated_at'], 'safe'],
            [['image', 'cropper650x400', 'cropper422x200'], 'required'],
            [['image', 'cropper650x400', 'cropper422x200'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'searchShortText' => 'Описание',
            'image' => 'Изображение 940x460',
            'cropper650x400' => 'Изображение 650x400',
            'cropper422x200' => 'Изображение 422x200',
        ]);
    }

    public function getTranslations()
    {
        return $this->hasMany(NewsTranslation::class, ['news_id' => 'id']);
    }

	public static function find()
	{
		$query = new NewsQuery(get_called_class());

		return $query;
	}

	public function getCurrentTranslation(): ActiveQuery
	{
		return $this->hasOne(NewsTranslation::class, ['news_id' => 'id'])
			->onCondition([NewsTranslation::tableName() . '.lang' => \Yii::$app->language]);
	}

	public function behaviors()
	{
		return [
			'translation' => self::behaviorsTranslation(NewsTranslation::class, 'news_id'),
			'timestamp' => self::behaviorsTimestamp(),
			'metaTag' => self::behaviorsMetaTag(
				'translation.title',
				'translation.short_text',
				self::class
			),
            'cropper-image' => [
                'class' => CropperBehavior::class,
                'attribute' => 'image',
                'uploadPath' => '@webroot/media/news/{id}',
                'path' => '@webroot/media/news/{id}',
                'url' => '@web/media/news/{id}',
                'fileName' => 'poster',
                'prefix' => '940x460-'
            ],
            'cropper-650x400' => [
                'class' => CropperBehavior::class,
                'attribute' => 'cropper650x400',
                'uploadPath' => '@webroot/media/news/{id}',
                'path' => '@webroot/media/news/{id}',
                'url' => '@web/media/news/{id}',
                'fileName' => 'poster',
                'prefix' => '650x400-'
            ],
            'cropper-422x200' => [
                'class' => CropperBehavior::class,
                'attribute' => 'cropper422x200',
                'uploadPath' => '@webroot/media/news/{id}',
                'path' => '@webroot/media/news/{id}',
                'url' => '@web/media/news/{id}',
                'fileName' => 'poster',
                'prefix' => '422x200-'
            ],
            'softDeleteBehavior' => [
                'class' => 'yii2tech\ar\softdelete\SoftDeleteBehavior',
                'softDeleteAttributeValues' => ['is_deleted' => 1],
                'invokeDeleteEvents' => false,
            ],
		];
	}

	public function getShortText()
	{
		return $this->currentTranslation->short_text;
	}

	public function getText()
	{
		return $this->currentTranslation->text;
	}

	public function getMainModelclass(): string
	{
		return self::class;
	}

    /**
     * Абсолютная ссылка на изображения для на главной
     *
     * @return string
     */
    public function getImageIndex()
    {
        return \Yii::$app->urlManagerAdmin->createUrl(['/media/news/' . $this->id . '/650x400-' . $this->image]);
    }

    /**
     * Абсолютная ссылка на изображения для списка
     *
     * @return string
     */
    public function getImagelist()
    {
        return \Yii::$app->urlManagerAdmin->createUrl(['/media/news/' . $this->id . '/422x200-' . $this->image]);
    }

    /**
     * Абсолютная ссылка на изображения
     *
     * @return string
     */
    public function getImageBig()
    {
        return \Yii::$app->urlManagerAdmin->createUrl(['/media/news/' . $this->id . '/940x460-' . $this->image]);
    }

    public function extraFields()
    {
        return ['translation'];
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->cropper650x400 = $this->image;
        $this->cropper422x200 = $this->image;
    }
}
