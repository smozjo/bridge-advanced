<?php

namespace common\models\handbooks\translation;

use common\core\CoreActiveRecord;
use common\models\handbooks\Handbooks;
use Yii;

/**
 * This is the model class for table "handbooks_translation".
 *
 * @property int $handbook_id
 * @property string $lang Язык перевода
 * @property string $title Заголовок
 * @property string $slug
 * @property string $description
 *
 * @property Handbooks $handbook
 */
class HandbooksTranslation extends CoreActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'handbooks_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lang', 'title'], 'required'],
            [['handbook_id'], 'integer'],
            [['description'], 'string'],
            [['lang', 'title', 'slug'], 'string', 'max' => 255],
            [['handbook_id', 'lang'], 'unique', 'targetAttribute' => ['handbook_id', 'lang']],
            [['handbook_id'], 'exist', 'skipOnError' => true, 'targetClass' => Handbooks::class, 'targetAttribute' => ['handbook_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHandbook()
    {
        return $this->hasOne(Handbooks::class, ['id' => 'handbook_id']);
    }

	public function behaviors()
	{
		return [
			'slug' => self::behaviorsSlug()
		];
	}
}
