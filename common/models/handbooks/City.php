<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 09.10.2019
 * Time 15:10
 */

namespace common\models\handbooks;


class City extends HandbookAbstract
{
	const ID = self::TYPE_CITIES;

	public function getUnremovableIds(): array
	{
		return [];
	}
}