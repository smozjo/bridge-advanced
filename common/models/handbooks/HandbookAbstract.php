<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 01.10.2019
 * Time 15:21
 */

namespace common\models\handbooks;


use common\models\handbooks\query\HandbooksQuery;
use common\models\handbooks\translation\HandbooksTranslation;
use yii\helpers\ArrayHelper;

abstract class HandbookAbstract extends Handbooks
{
	protected $virtualProperties = null;

	abstract public function getUnremovableIds(): array ;

	public function init()
	{
		parent::init();
		$this->type = static::ID;
	}

	public static function find()
	{
		$query = new HandbooksQuery(get_called_class());
		return $query->byType(static::ID, static::ALIAS);
	}

	public static function getDropDownData()
	{
		return self::getDropdownList('title', 'id', ['methods' => [
			['currentTranslation']
		]]);
	}

	public static function getOrSet($params): Handbooks
	{
		$handbook = static::find()
			->currentTranslation()
			->andWhere(ArrayHelper::merge(
				['AND'],
				array_map(function($key) use ($params){
					return [$key => $params[$key]];
				}, array_keys($params))
			))
			->one();

		if($handbook) {
			return $handbook;
		}

		$handbook = new static([
			'type' => static::ID,
			'is_active' => self::STATE_ACTIVE
		]);
		$handbook->save();

		foreach (\Yii::$app->getModule('admin')->languages as $code => $lang) {
			(new HandbooksTranslation([
				'handbook_id' => $handbook->id,
				'lang' => $code,
				'title' => $params['title']
			]))->save();
		}

		return $handbook;
	}

	public function beforeSave($insert)
	{
		$this->some_fields = $this->virtualProperties;
		return parent::beforeSave($insert);
	}

	public function __get($name)
	{
		if(isset($this->virtualProperties[$name])) {
			return $this->some_fields[$name]??$this->virtualProperties[$name];
		}
		return parent::__get($name);
	}

	public function __set($name, $value)
	{
		if(isset($this->virtualProperties[$name])) {
			$this->virtualProperties[$name] = $value;
		} else {
			parent::__set($name, $value);
		}
	}
}