<?php
/**
 * Created by PhpStorm.
 * User: prokhonenkov
 * Date: 10.12.18
 * Time: 18:36
 */

namespace common\models\handbooks;


use common\components\interfaces\translations\MainTranslationInterface;

interface HandbooksTranslationInterface extends MainTranslationInterface
{
	/**
	 * @return string
	 */
	public function getSlug(): ?string;

}