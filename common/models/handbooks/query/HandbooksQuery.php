<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 30.09.2019
 * Time 17:00
 */

namespace common\models\handbooks\query;


use common\core\CacheActiveQuery;

class HandbooksQuery extends CacheActiveQuery
{
	public function init()
	{
		parent::init();
	}

	public function byType($type, $alias = null)
	{
		return $this->andOnCondition([$alias. '.type' => $type]);
	}

	public function withTranslations()
	{
		return $this->joinWith('translations');
	}

}