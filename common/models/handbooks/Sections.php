<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 01.10.2019
 * Time 15:00
 */

namespace common\models\handbooks;

class Sections extends HandbookAbstract
{
	const ID = self::TYPE_SECTIONS;

	const MAIN_PAGE = 9;
	const COMPLEX = 22;
	const BLOCK = 23;
	const NEWS = 24;
	const PLANS = 25;
	const PAGES = 26;

	public function getUnremovableIds(): array
	{
		return [
			self::MAIN_PAGE,
			self::COMPLEX,
			self::BLOCK,
			self::NEWS,
			self::PLANS,
			self::PAGES,
		];
	}

}