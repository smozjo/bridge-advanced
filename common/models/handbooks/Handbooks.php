<?php

namespace common\models\handbooks;

use common\components\behaviors\AutoFillTranslationForm;
use common\components\helpers\StaticList;
use common\components\interfaces\ParentClassInterface;
use common\core\CoreActiveRecord;
use common\models\handbooks\translation\HandbooksTranslation;
use common\models\handbooks\query\HandbooksQuery;
use app\modules\movies\models\Movies;
use app\modules\movies\models\MoviesHandbooks;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "handbooks".
 *
 * @property int $id
 * @property int $type
 * @property int $position
 * @property int $is_active
 * @property string $image
 * @property string $some_fields
 *
 * @property HandbooksTranslation[] $translations
 */
class Handbooks extends CoreActiveRecord implements HandbooksTranslationInterface
{
	const ALIAS = 'handbooks';
	const TYPE_CITIES = 1;
	const TYPE_SECTIONS = 2;


	private static $types = [
		self::TYPE_CITIES => 'Города',
		self::TYPE_SECTIONS => 'Разделы',
	];

	private static $typesClassMap = [
	];

	protected static $defaultColumns = [
		'title',
		'slug',
		'is_active'
	];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'handbooks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'position', 'is_active'], 'integer'],
            [['image'], 'image'],
			[['some_fields'], 'safe']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(HandbooksTranslation::class, ['handbook_id' => 'id']);
    }

	public function getCurrentTranslation($alias = null): ActiveQuery
	{
		$alias = static::ALIAS . 'Translation';

		return $this->hasOne(HandbooksTranslation::class, ['handbook_id' => 'id'])
			->onCondition([$alias . '.lang' => \Yii::$app->language])
			->alias($alias);
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'metaTag' => self::behaviorsMetaTag('translation.title', 'translation.title'),
			'translation' => self::behaviorsTranslation(
				HandbooksTranslation::class,
				'handbook_id'
			),
			'positionSort' => self::behaviorsPositionSort(
				'position', ['type']
			),
			'autoFillTranslationForm' => [
				'class' => AutoFillTranslationForm::class,
				'excludeAttributes' => ['handbook_id']
			],
			'image' => self::behaviorsImageUpload(
				'image',
				'@webroot/media/handbooks/{id}',
				'@web/media/handbooks/{id}',
				[
				]
			),
		]);
	}

	public static function find()
	{
		return new HandbooksQuery(get_called_class());
	}

	public static function getTypeTitle(int $type_id = null)
	{
		return StaticList::getStaticTitle(
			'type',
			self::$types,
			'bridge',
			$type_id
		);
	}

	public static function getTypes()
	{
		return StaticList::getList(self::$types, 'bridge');
	}

	public static function getExcludeRemoveIds($type): array
	{
		if(!isset(self::$typesClassMap[$type])) {
			return [];
		}
		return (new self::$typesClassMap[$type])->getUnremovableIds();
	}

	public static function getClass()
	{
		return self::$typesClassMap[\Yii::$app->request->get('type')]??null;
	}

	public static function isDefaultColumn($column): bool
	{
		return in_array($column, static::$defaultColumns);
	}
}
