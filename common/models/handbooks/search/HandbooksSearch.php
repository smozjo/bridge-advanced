<?php

namespace common\models\handbooks\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\handbooks\Handbooks;

/**
 * HandbooksSearch represents the model behind the search form of `common\models\handbooks\Handbooks`.
 */
class HandbooksSearch extends Handbooks
{
	public $searchTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'position', 'is_active'], 'integer'],
			[['searchTitle'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Handbooks::find()
			->currentTranslation()
			->byType(\Yii::$app->request->get('type'));

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'title', $this->searchTitle])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

        return $dataProvider;
    }
}
