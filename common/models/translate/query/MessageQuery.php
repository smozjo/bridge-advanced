<?php

namespace common\models\translate\query;

/**
 * This is the ActiveQuery class for [[\common\models\translate\models\Message]].
 *
 * @see \common\models\translate\models\Message
 */
class MessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\translate\models\Message[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\translate\models\Message|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
