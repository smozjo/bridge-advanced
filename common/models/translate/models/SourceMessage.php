<?php

namespace common\models\translate\models;

use Yii;

/**
 * This is the model class for table "source_message".
 *
 * @property int $id
 * @property string|null $category
 * @property string|null $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'category' => Yii::t('app', 'Категория'),
            'message' => Yii::t('app', 'Сообщение'),
        ];
    }

    /**
     * Gets query for [[Messages]].
     *
     * @return \yii\db\ActiveQuery|\common\models\translate\query\MessageQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::class, ['id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\translate\query\SourceMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\translate\query\SourceMessageQuery(get_called_class());
    }
}
