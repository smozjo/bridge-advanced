<?php
namespace common\fixtures;

use common\models\AuthItem;
use yii\test\ActiveFixture;

class AuthItemFixture extends ActiveFixture
{
    public $modelClass = AuthItem::class;
}
