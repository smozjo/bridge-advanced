<?php
namespace common\fixtures;

use common\models\AuthAssignment;
use yii\test\ActiveFixture;

class AuthAssignmentFixture extends ActiveFixture
{
    public $modelClass = AuthAssignment::class;
}
