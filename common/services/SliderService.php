<?php

namespace common\services;

class SliderService
{
    const MAIN_HEADER_SLIDER = 1;
    const MAIN_FOOTER_SLIDER = 2;
    const ABOUT_US_SLIDER = 3;
}
