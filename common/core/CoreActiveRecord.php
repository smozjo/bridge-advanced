<?php

namespace common\core;

use common\components\behaviors\BridgeUploadImageBehavior;
use common\components\bridge\MetaTagBehavior;
use common\components\bridge\MetaTagsComponent;
use Bridge\Core\Behaviors\TranslationBehavior;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Application;

class CoreActiveRecord extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'is_active' => \Yii::t('app', 'Активный'),
			'slug' => \Yii::t('app', 'Seo ссылка'),
			'title' => \Yii::t('app', 'Название'),
			'searchTitle' => \Yii::t('app', 'Заголовок'),
			'link' => \Yii::t('app', 'Ссылка'),
			'kind_id' => \Yii::t('app', 'Вид'),
			'description' => \Yii::t('app', 'Описание'),
			'city_id' => \Yii::t('app', 'Город'),
			'type' => \Yii::t('app', 'Тип'),
			'type_id' => \Yii::t('app', 'Тип'),
			'phone' => \Yii::t('app', 'Телефон'),
			'email' => \Yii::t('app', 'Email'),
			'image' => \Yii::t('app', 'Изображение'),
			'expired_at' => \Yii::t('app', 'Дата истечения'),
			'serchFullName' => \Yii::t('app', 'Пользователь'),
			'short_description' => \Yii::t('app', 'Полное описание') ,
			'price' => \Yii::t('app', 'Цена') ,
			'hits_count' => \Yii::t('app', 'Просмотры') ,
			'published_at' => \Yii::t('app', 'Дата публикации') ,
			'author_id' => \Yii::t('app', 'Автор') ,
			'short_text' => \Yii::t('app', 'Краткое описание') ,
			'text' => \Yii::t('app', 'Описание') ,
			'postCategory' => \Yii::t('app', 'Категории') ,
			'category_id' => \Yii::t('app', 'Категории') ,
			'age' => \Yii::t('app', 'Возраст') ,
			'tagNames' => \Yii::t('app', 'Метки') ,
			'status' => \Yii::t('app', 'Статус'),
			'question' => \Yii::t('app', 'Вопрос'),
			'answer' => \Yii::t('app', 'Ответ'),
			'user_id' => \Yii::t('app', 'Пользователь'),
			'created_at' => \Yii::t('app', 'Дата создания'),
			'updated_at' => \Yii::t('app', 'Дата последнего редактирования'),
			'video' => \Yii::t('app', 'Видео'),
			'address' => \Yii::t('app', 'Адрес'),
			'contacts' => \Yii::t('app', 'Контакты'),
			'file' => \Yii::t('app', 'Выбрать файл'),
			'position' => \Yii::t('app', 'Очередность'),
			'pushText' => \Yii::t('app', 'Текст'),
			'subtitle' => \Yii::t('app', 'Подзаголовок'),
			'pushTitle' => \Yii::t('app', 'Заголовок'),
			'isPush' => \Yii::t('app', 'Отправить пуш при сохранении'),
			'logo' => \Yii::t('app', 'Лого'),
			'background' => \Yii::t('app', 'Фон'),
			'style' => \Yii::t('app', 'Стиль'),
			'userName' => \Yii::t('app', 'Пользолватель'),
			'quantity' => \Yii::t('app', 'Количество'),
			'seo_text' => \Yii::t('app', 'Сео текст'),
			'place' => \Yii::t('app', 'Место'),
			'name' => \Yii::t('app', 'Имя'),
			'duration' => \Yii::t('app', 'Продолжительность'),
			'language_id' => \Yii::t('app', 'Язык'),
			'year' => \Yii::t('app', 'Год'),
			'birthday' => \Yii::t('app', 'День рождения'),
			'photo' => \Yii::t('app', 'Фото'),
			'views' => \Yii::t('app', 'Просмотры'),
			'publish_at' => \Yii::t('app', 'Дата публикации'),
			'finished_at' => \Yii::t('app', 'Дата завершения'),
		];
	}

    const STATE_ACTIVE = 1;
    const STATE_INACTIVE = 0;

    public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
		if( \Yii::$app->bridge->isAdmin) {
			\Yii::$app->cache->flush();
		}
	}

	public function afterDelete()
	{
		parent::afterDelete();

		if( \Yii::$app->bridge->isAdmin) {
			\Yii::$app->cache->flush();
		}
	}

	public static function getDropdownList($title = 'title', $key = 'id', array $params = [])
    {

    	$params = ArrayHelper::merge([
			'condition' => [],
			'order' => ['title' => SORT_ASC],
			'select' => [],
			'join' => [],
			'asArray' => true,
			'limit' => null
		], $params);

        $query = static::find();

        $query->select([
            'title' => $title,
            'id' => $key
        ]);

        if (!empty($params['condition'])) {
            $query->andFilterWhere($params['condition']);
        }

        if (!empty($params['select'])) {
            $query->select($params['select']);
        }

        if (!empty($params['join'])) {
            $query->joinWith($params['join'], false);
        }

        if (!empty($params['asArray']) && $params['asArray']) {
            $query->asArray();
        }

        if (!empty($params['limit']) && $params['limit']) {
            $query->limit($params['limit']);
        }

        if (!empty($params['methods']) && $params['methods']) {
			foreach ($params['methods'] as $methodParams) {
				$methodName = array_shift($methodParams);
				call_user_func_array([$query, $methodName], $methodParams);
			}
        }

        return $query
			->distinct()
            ->indexBy('id')
            ->orderBy($params['order'])
            ->column();
    }

	public static function query($sql = null)
	{
		$connection = \Yii::$app->db;
		return $connection->createCommand($sql);
	}

	/**
	 * @return string
	 */
	public function getTitle(): ?string
	{

		if($this->hasMethod('getCurrentTranslation') && $this->currentTranslation) {
			return $this->currentTranslation->title;
		}
		return $this->title??null;
	}

	/**
	 * @return string
	 */
	public function getLang(): string
	{
		if($this->hasMethod('getCurrentTranslation') && $this->currentTranslation) {
			return $this->currentTranslation->lang;
		}
		return '';
	}

	/**
	 * @return string
	 */
	public function getSlug(): ?string
	{
		if($this->hasMethod('getCurrentTranslation') && $this->currentTranslation) {
			return $this->currentTranslation->slug;
		}
		return $this->slug??null;
	}

	protected static function behaviorsSlug($nameAttribute = 'title', $slugAttribute = 'slug', $ensureUnique = false)
	{
		return [
			'class' => 'Bridge\Slug\BridgeSlugBehavior',
			'slugAttribute' => $slugAttribute,
			'attribute' => $nameAttribute,
			'ensureUnique' => $ensureUnique,
			'replacement' => '-',
			'lowercase' => 1,
			'immutable' => 1,
			'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
		];
	}

	protected static function behaviorsTimestamp()
	{
		return  [
			'class' => TimestampBehavior::class,
			'attributes' => [
				self::EVENT_BEFORE_INSERT => ['created_at'],
				self::EVENT_BEFORE_UPDATE => ['updated_at'],
			],
			'value' => new Expression('NOW()'),
		];
	}

	protected static function behaviorsTimestampCreated()
	{
		return  [
			'class' => TimestampBehavior::class,
			'attributes' => [
				self::EVENT_BEFORE_INSERT => ['created_at'],
			],
			'value' => new Expression('NOW()'),
		];
	}

	protected static function behaviorsUserId()
	{
		return   [
			'class' => AttributeBehavior::class,
			'attributes' => [
				ActiveRecord::EVENT_BEFORE_INSERT => ['user_id'],
			],
			'value' => \Yii::$app->user->getId(),
		];
	}

	protected static function behaviorsPositionSort($attribute = 'position', $groupAttributes = [])
	{
		return [
			'class' => 'yii2tech\ar\position\PositionBehavior',
			'positionAttribute' => $attribute,
			'groupAttributes' => $groupAttributes,
		];
	}

	protected static function behaviorsTranslation($class, $attribute)
	{
		return [
			'class' => TranslationBehavior::class,
			'translationModelClass' => $class,
			'translationModelRelationColumn' => $attribute
		];
	}

	protected static function behaviorsMetaTag($titleColumn, $descriptionColumn, $className = null)
	{
		return [
			'class' => MetaTagBehavior::class,
			'titleColumn' => $titleColumn,
			'descriptionColumn' => $descriptionColumn,
			'className' => $className,
		];
	}
	protected static function behaviorsImageUpload($attribute, $path, $url, $thumbs, $isTranslation = false, $instanceByName = false)
	{
		return [
			'class' => BridgeUploadImageBehavior::class,
			'attribute' => $attribute,
			'path' => $path,
			'url' => $url,
			'isTranslation' => $isTranslation,
			'instanceByName' => $instanceByName,
			'scenarios' => ['create', 'update', 'default'],
			'thumbs' => $thumbs,
		];
	}

	public function registerMetaTag(): void
	{
		$component = new MetaTagsComponent();
		$component->registerModel($this);
	}

	public function updateAttributes($attributes)
	{
		\Yii::$app->cache->flush();
		return parent::updateAttributes($attributes);
	}
}