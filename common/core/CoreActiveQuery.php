<?php
namespace common\core;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

class CoreActiveQuery extends ActiveQuery
{
    public function active($alias = null, $field = 'is_active'): self
	{
        $alias = $alias ?: $this->tableName();
        return $this->andWhere([$alias . '.' . $field => CoreActiveRecord::STATE_ACTIVE]);
    }

    public function bySlug($slug, $alias = null, $field = 'slug'): self
	{
		return $this->byField($slug, $field, $alias);
    }

    public function byId($id, $alias = null, $field = 'id'): self
	{
        return $this->byField($id, $field, $alias);
    }

    public function byUserId($id, $alias = null, $field = 'user_id'): self
	{
        return $this->byField($id, $field, $alias);
    }

    public function my(): self
	{
        return $this->byUserId(\Yii::$app->user->id);
    }

    public function published($alias = null, $field = 'publish_at'): self
	{
        $alias = $alias ?: $this->tableName();
        return $this->andWhere(['<', $alias . '.' . $field, new Expression('NOW()')]);
    }

    public function notExpired($alias = null, $field = 'expired_at'): self
	{
        $alias = $alias ?: $this->tableName();
        return $this->andWhere(['OR',
        	['IS', $alias . '.' . $field, null],
			['>', $alias . '.' . $field, new Expression('NOW()')]
		]);
    }

    public function currentTranslation($eagerLoading = true): self
	{
		return $this->joinWith(['currentTranslation'], $eagerLoading);
	}

	public function positionAsc($alias = null)
	{
		$alias = $alias ?: $this->tableName();
		return $this->orderBy([$alias . '.position' => SORT_ASC]);
	}

	public function publishAsc($alias = null)
	{
		$alias = $alias ?: $this->tableName();
		return $this->orderBy([$alias . '.publish_at' => SORT_ASC]);
	}

	public function publishDesc($alias = null)
	{
		$alias = $alias ?: $this->tableName();
		return $this->orderBy([$alias . '.publish_at' => SORT_DESC]);
	}

	protected function byField($value, $field, $alias = null): self
	{
		$alias = $alias ?: $this->tableName();
		return $this->andWhere([$alias . '.' . $field => $value]);
	}

	protected function tableName()
    {
        /** @var ActiveRecord $modelClass */
        $modelClass = $this->modelClass;
        return $modelClass::tableName();
    }
}