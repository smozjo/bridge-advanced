<?php

namespace common\core;

use Bridge\Core\Controllers\BaseAdminController;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class CoreAdminController extends BaseAdminController
{
	public function actionDeleteGalleryFile(string $modelName)
	{
		$id = \Yii::$app->request->post('key');
		$model = urldecode($modelName)::findOne($id);

		if (!$model) {
			throw new NotFoundHttpException();
		}

		$path = $model::getDir() . DIRECTORY_SEPARATOR . $model->path;

		if($model->delete() && unlink($path)) {
			return true;
		}
		return false;
	}

	public function actionSortImage($postId, $modelName)
	{
		/** @var $modelName GalleryAbstract */

		if (\Yii::$app->request->isAjax) {
			$post = \Yii::$app->request->post('position');

			if ($post['oldIndex'] > $post['newIndex']) {
				$param = ['and', ['>=', 'position', $post['newIndex']], ['<', 'position', $post['oldIndex']]];
				$counter = 1;
			} else {
				$param = ['and', ['<=', 'position', $post['newIndex']], ['>', 'position', $post['oldIndex']]];
				$counter = -1;
			}

			$modelName = urldecode($modelName);

			$modelName::updateAllCounters(['position' => $counter], [
				'and', [$modelName::getRelationIdFieldName() => $postId], $param
			]);

			$modelName::updateAll(['position' => $post['newIndex']], [
				'id' => $post['stack'][$post['newIndex']]['key']
			]);

			return true;
		}

		throw new BadRequestHttpException();
	}

	public function actionAutofill(string $q, string $className, $fieldName = 'title')
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		$className = urldecode($className);

		$data =  $className::getDropdownList($fieldName, 'id', ['methods' => [
			['currentTranslation'],
			['limit', 20],
			['where', ['LIKE', $fieldName, $q]],
			['andFilterWhere', ArrayHelper::merge(['AND'], array_map(function ($key) {
				return [$key => \Yii::$app->request->get('params')[$key]];
			}, array_keys((array)\Yii::$app->request->get('params')) ))]
		]]);

		$out['results'] = [];

		foreach ($data as $key => $value) {
			$out['results'][] = [
				'id' => $key,
				'text' => $value
			];
		}

		return  $out;
	}

	public function actionRemove($id)
	{
		$class = $this->modelClass;
		/** @var ActiveRecord $model */
		$model = $class::findOne($id);
		return $model->delete();

	}
}