<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 19.09.2019
 * Time 20:30
 */

namespace common\core;


use Bridge\Core\Widgets\ActiveForm;

class CoreActiveForm extends ActiveForm
{
	public $fieldClass = 'common\core\CoreActiveField';
}