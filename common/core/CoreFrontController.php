<?php

namespace common\core;

use admin\modules\menu\components\behaviors\MenuBehavior;
use admin\modules\menu\components\MenuConfigurator;
use common\models\menu\Menu;
use admin\modules\settings\components\behaviors\SettingsBehavior;
use admin\modules\user\models\UserDevices;
use Da\User\Validator\AjaxRequestModelValidator;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Cookie;

class CoreFrontController extends Controller
{
	public $bodyClass = null;

	public $languageRouteParams = [];

	public function init()
	{
		parent::init();


		/*if(\Yii::$app->request->url == '/index.php') {
			header('Location: /', 301);
			exit;
		}*/

		MenuConfigurator::getInstance()
			->setMenuId(Menu::MENU_MAIN)
			->setMenuId(Menu::MENU_SOCIAL)
			->setMenuId(Menu::MENU_APPS)
			->setMenuId(Menu::MENU_FOOTER)
			->setMenuId(Menu::MENU_MOBILE_FOOTER)
            ->setMenuId(Menu::MENU_PROFILE_HEADER)
            ->setMenuId(Menu::MENU_FILTER_MOVIES);

		if(UserDevices::getOrSet()->isDisabled()) {
			\Yii::$app->getUser()->logout();
		}
	}


	public function actionAjaxValidate($model)
	{
		$model = new $model;
		(new AjaxRequestModelValidator($model))->validate();
	}

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'settings' => [
				'class' => SettingsBehavior::class,
			],
			'menu' => [
				'class' => MenuBehavior::class
			]
		]);
	}
}