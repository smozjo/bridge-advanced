<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 19.09.2019
 * Time 20:31
 */

namespace common\core;


use Bridge\Core\Widgets\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class CoreActiveField extends ActiveField
{
	public function imageUpload($behaviorName = 'imageUpload', $options = [])
	{
		$initialPreview = [];
		if (!empty($this->getUploadUrl())) {
			$initialPreview[] = Html::img($this->getUploadUrl(), [
				'class' => 'file-preview-image',
				'title' => $this->model->getAttributeLabel($this->attribute),
				'alt' => $this->model->getAttributeLabel($this->attribute),
				'style' => 'max-height: 170px;'
			]);
		}

		$deleteUrlOption = isset($this->model->id) ? [
			'deleteUrl' => Url::to([
				'/admin/base-admin/delete-file',
				'id' => $this->model->id,
				'modelName' => get_class($this->model),
				'behaviorName' => $behaviorName
			])
		] : [];

		return $this->fileUpload(ArrayHelper::merge([
			'pluginOptions' => ArrayHelper::merge([
				'showUpload' => false,
				'showRemove' => false,
				'initialPreview' => $initialPreview,
			], $deleteUrlOption)
		], $options));
	}
}