<?php
/**
 * Created by PhpStorm.
 * User: prokhonenkov
 * Date: 25.02.19
 * Time: 15:31
 */

namespace common\core;


use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

abstract class RepeaterActiveRecordAbstract extends CoreActiveRecord
{
	/**
	 * @return string
	 */
	abstract static function getItemsFieldName(): ?string ;

	/**
	 * @return string
	 */
	abstract static function getHoldersFieldName(): string ;

	/**
	 * @return array
	 */
	abstract static function getFileAttributes(): array ;

	/**
	 * @param int $holderId
	 * @return array
	 */
	public static function getExistData(int $holderId): array
	{
		return static::find()
			->where([static::getHoldersFieldName() => $holderId])
			->asArray()
			->all();
	}

	/**
	 * @param int $holderId
	 * @param array $data
	 * @param bool $isDelete
	 * @return bool
	 * @throws \yii\base\Exception
	 * @throws \yii\db\Exception
	 */
	public static function batchUpdateCustom(int $holderId, array $data, array $deleteCondition = [], bool $isDelete = true): bool
	{
		return self::updateData($holderId, $data, $isDelete, $deleteCondition);
	}

	/**
	 * @param int $holderId
	 * @param string $formName
	 * @param bool $isDelete
	 * @return bool
	 * @throws \yii\base\Exception
	 * @throws \yii\db\Exception
	 */
	public static function batchUpdate(int $holderId, string $formName, bool $isDelete = true): bool
	{
		return self::updateData($holderId, $formName, $isDelete);
	}

	/**
	 * @param int $holderId
	 * @param $formName
	 * @param bool $isDelete
	 * @return bool
	 * @throws \yii\base\Exception
	 * @throws \yii\db\Exception
	 */
	private static function updateData(int $holderId, $formName, $isDelete = true, array $deleteCondition = []): bool
	{
		if(is_array($formName)) {
			$data = $formName;
		} else {
			$data = (array)\Yii::$app->request->post($formName);
		}

		if($data) {
			$fields = ArrayHelper::merge(
				array_keys($data[key($data)]),
				[static::getHoldersFieldName()]
			);
		}

		$existsData = static::getExistData($holderId);

		$values = [];

		$uploadFiles = [];
		$existingFiles = [];

		$fileDir = \Yii::getAlias('@webroot/media/' .
			static::tableName() .
			'/' . $holderId
		);

		foreach ($data as $index => $datum) {
			foreach (static::getFileAttributes() as $fileAtttribute) {

				$file = UploadedFile::getInstanceByName("{$formName}[{$index}][{$fileAtttribute}]");

				if(!is_array($formName)) {
					if($file) {

						$fileName = md5(microtime()) . '.' . $file->getExtension();

						FileHelper::createDirectory($fileDir);

						$file->saveAs($fileDir . '/' . $fileName);

						$datum[$fileAtttribute] = $fileName;

						$uploadFiles[$fileName] = $fileDir . '/' . $fileName;
					}
				}

				if($datum[$fileAtttribute]) {
					$existingFiles[$fileAtttribute][] = $datum[$fileAtttribute];
				}
			}

			if(static::getItemsFieldName()) {
				$index = $datum[static::getItemsFieldName()];
			}

			$values[$index] = ArrayHelper::merge(
				array_values($datum),
				[$holderId]
			);
		}

		$deleteFiles = [];
		foreach (static::getFileAttributes() as $fileAtttribute) {
			foreach($existsData as $existsDatum) {
				if(!isset($existingFiles[$fileAtttribute]) || !in_array($existsDatum[$fileAtttribute], $existingFiles[$fileAtttribute])) {
					$deleteFiles[] = $existsDatum[$fileAtttribute];
				}
			}
		}

		$transaction = static::getDb()->beginTransaction();

		if($isDelete) {
			static::deleteAll(['AND', ArrayHelper::merge(
				[static::getHoldersFieldName() => $holderId],
				$deleteCondition
			)]);
		}

		try {
			$insert = false;
			if($values) {
				$insert = self::query()
					->batchInsert(static::tableName(), $fields, $values)
					->execute();
			}
		} catch (\Exception $e) {
			$transaction->rollBack();
			if($uploadFiles) {
				foreach ($uploadFiles as $uploadFile) {
					try {
						unlink($uploadFile);
					} catch (\Exception $e) {

					}
				}
			}
			return false;
		}

		$transaction->commit();

		foreach ($deleteFiles as $file) {
			try {
				unlink($fileDir . '/' . $file);
			} catch (\Exception $e) {

			}
		}

		return $insert;
	}

	/**
	 * @param $attribute
	 * @return string
	 */
	public function getFileUrl($attribute): ?string
	{
		if(!$this->$attribute) {
			return null;
		}
		return '/media/' .
			'/' . static::tableName() .
			'/' . $this->{static::getHoldersFieldName()} .
			'/' . $this->$attribute;
	}
}