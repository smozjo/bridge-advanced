<?php
namespace common\core;

use yii\console\Application;

class CacheActiveQuery extends CoreActiveQuery
{
	const DEFAULT_CACHE_DURATION = 3600;

	public function all($db = null)
	{
		if(\Yii::$app instanceof Application    ) {
			return  parent::all($db);
		}
		$cacheName = 'all_' . $this->getCacheName();

		$result = \Yii::$app->getCache()->get($cacheName);

		if ($result === false) {
			$result = parent::all($db);


			\Yii::$app->getCache()->set($cacheName, $result, self::DEFAULT_CACHE_DURATION);
		}

		return $result;
	}

	public function one($db = null)
	{
		if(\Yii::$app instanceof Application) {
			return  parent::one($db);
		}
		$cacheName = 'one_' . $this->getCacheName();
		$reasult = \Yii::$app->getCache()->get($cacheName);
		if ($reasult === false) {
			$reasult = parent::one($db);;
			\Yii::$app->getCache()->set($cacheName, $reasult, self::DEFAULT_CACHE_DURATION);
		}

		return $reasult;
	}

	private function getCacheName()
	{
		return md5($this->createCommand()->rawSql . '-' . (int)is_null(\Yii::$app->getModule('v1')));
	}

}