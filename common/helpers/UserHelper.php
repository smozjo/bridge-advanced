<?php


namespace common\helpers;


class UserHelper
{
    public static function isAdmin()
    {
        if(!empty(\Yii::$app->user->id) && \Yii::$app->user->getIdentity()->hasRole('admin')){
            return \Yii::$app->user->getIdentity()->hasRole('admin');
        }
        return false;
    }
}