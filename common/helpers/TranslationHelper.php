<?php

namespace common\helpers;

use common\models\translate\models\Message;
use common\models\translate\models\SourceMessage;
use yii\i18n\MissingTranslationEvent;

class TranslationHelper
{
    /**
     * @param MissingTranslationEvent $event
     */
    public static function missingTranslation(MissingTranslationEvent $event)
    {
        $sourceMessage = SourceMessage::find()
            ->where('category = :category and message = :message', [
                ':category' => $event->category,
                ':message' => $event->message
            ])
            ->with('messages')
            ->one();

        if (!$sourceMessage) {
            $sourceMessage = new SourceMessage;
            $sourceMessage->setAttributes([
                'category' => $event->category,
                'message' => $event->message
            ], false);
            $sourceMessage->save(false);

            foreach(LanguageHelper::getLanguages() as $language){
                $message = new Message;
                $message->setAttributes([
                    'id' => $sourceMessage->id,
                    'language' => $language,
                ], false);
                $message->save(false);
            }
        }
    }

    public static function t($category, $message)
    {
        if(UserHelper::isAdmin()){
            $sourceMessage = SourceMessage::find()
                ->where('category = :category and message = :message', [
                    ':category' => $category,
                    ':message' => $message
                ])
                ->with('messages')
                ->one();
            if(!empty($sourceMessage)){
                return \Yii::t($category, $message) . '<a target="blank" href="' . \Yii::$app->urlManagerAdmin->createUrl(['/admin/i18n/default/update?id=' . $sourceMessage->id]) . '"><sup><img src="/media/edit.jpg"></sup></a>';
            }
        }
        return \Yii::t($category, $message);
    }
}