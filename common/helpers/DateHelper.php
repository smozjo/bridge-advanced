<?php


namespace common\helpers;

use DateTime;

class DateHelper
{
    public static function getMonth() : array
    {
        return [
            '1' =>  \Yii::t('app','Январь'),
            '2' =>  \Yii::t('app','Февраль'),
            '3' =>  \Yii::t('app','Март'),
            '4' =>  \Yii::t('app','Апрель'),
            '5' =>  \Yii::t('app','Май'),
            '6' =>  \Yii::t('app','Июнь'),
            '7' =>  \Yii::t('app','Июль'),
            '8' =>  \Yii::t('app','Август'),
            '9' =>  \Yii::t('app','Сентябрь'),
            '10' => \Yii::t('app','Ноябрь'),
            '11' => \Yii::t('app','Октябрь'),
            '12' => \Yii::t('app','Декабрь'),
        ];
    }

    public static function getMonthGenitive() : array
    {
        return [
            '1' =>  \Yii::t('app','Января'),
            '2' =>  \Yii::t('app','Февраля'),
            '3' =>  \Yii::t('app','Марта'),
            '4' =>  \Yii::t('app','Апреля'),
            '5' =>  \Yii::t('app','Мая'),
            '6' =>  \Yii::t('app','Июня'),
            '7' =>  \Yii::t('app','Июля'),
            '8' =>  \Yii::t('app','Августа'),
            '9' =>  \Yii::t('app','Сентября'),
            '10' => \Yii::t('app','Ноября'),
            '11' => \Yii::t('app','Октября'),
            '12' => \Yii::t('app','Декабря'),
        ];
    }

    public static function getYears() : array
    {
        $year = date('Y');
        $arr = [];
        for ($i = 1; $i <= 5; $i++) {
            $arr[$year] = $year;
            $year--;
        }
        return $arr;
    }

    public static function format($dateTime)
    {
        $mount_number = DateTime::createFromFormat('Y-m-d H:i:s', $dateTime)->format('n');
        return DateTime::createFromFormat('Y-m-d H:i:s', $dateTime)->format('j '
            . self::getMonthGenitive()[$mount_number] . ' Y');
    }
}