<?php

namespace common\helpers;

class NumberHelper
{
    /**
     * @param int $n - число
     * @param array $words - массив слов пример ['яблоко','яблока','яблок']
     * @return string
     */
    public static function declension_words(int $n, array $words): string
    {
        return ($words[($n=($n=$n%100)>19?($n%10):$n)==1?0 : (($n>1&&$n<=4)?1:2)]);
    }
}