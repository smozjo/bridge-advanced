<?php

namespace common\helpers;

class LanguageHelper
{
    const EN = 'en-US';
    const RU = 'ru-RU';
    const KK = 'kk-KZ';

    const ALIAS_EN = 'en';
    const ALIAS_RU = 'ru';
    const ALIAS_KK = 'kz';

    const LABEL_EN = 'English';
    const LABEL_RU = 'Русский';
    const LABEL_KK = 'Қазақ';

    public static function getLanguages() : array
    {
        return [
            self::ALIAS_EN => self::EN,
            self::ALIAS_RU => self::RU,
            self::ALIAS_KK => self::KK,
        ];
    }

    public static function getLanguagesLabels() : array
    {
        return [
            self::EN => self::LABEL_EN,
            self::RU => self::LABEL_RU,
            self::KK => self::LABEL_KK,
        ];
    }

    public static function getLabelByLanguage(string $language) : string
    {
        return self::getLanguagesLabels()[$language];
    }
}