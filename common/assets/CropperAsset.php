<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CropperAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
		'js/cropper/scripts/jquery.Jcrop.js',
		'js/cropper/scripts/jquery.SimpleCropper.js',
    ];

    public $css = [
		'js/cropper/css/style.css',
		'js/cropper/css/style-example.css',
		'js/cropper/css/jquery.Jcrop.css',
	];

    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
