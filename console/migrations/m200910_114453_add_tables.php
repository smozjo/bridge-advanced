<?php

use yii\db\Migration;

/**
 * Class m200910_114453_add_tables
 */
class m200910_114453_add_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . '/../db/dump.sql'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200910_114453_add_tables cannot be reverted.\n";

        return false;
    }
}
