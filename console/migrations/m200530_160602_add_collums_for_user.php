<?php

use yii\db\Migration;

/**
 * Class m200530_160602_add_collums_for_user
 */
class m200530_160602_add_collums_for_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'verification_token', $this->string()->defaultValue(null));
        $this->addColumn('{{%user}}','status', $this->smallInteger()->notNull()->defaultValue(10));
        $this->addColumn('{{%user}}','password_reset_token', $this->string()->unique());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'verification_token');
        $this->dropColumn('{{%user}}', 'status');
        $this->dropColumn('{{%user}}', 'password_reset_token');
    }
}
