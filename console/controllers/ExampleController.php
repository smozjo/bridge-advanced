<?php
namespace console\controllers;

use yii\console\Controller;
use yii\console\ExitCode;

class ExampleController extends Controller
{
    public function actionIndex()
    {
        return ExitCode::OK;
    }
}