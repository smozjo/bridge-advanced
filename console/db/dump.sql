-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 10 2020 г., 12:54
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


--
-- Структура таблицы `handbooks`
--

CREATE TABLE `handbooks` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL,
  `position` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `image` varchar(255) DEFAULT NULL,
  `some_fields` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `handbooks_translation`
--

CREATE TABLE `handbooks_translation` (
  `handbook_id` int(11) UNSIGNED NOT NULL,
  `lang` varchar(255) NOT NULL COMMENT 'Язык перевода',
  `title` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `slug` varchar(255) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `is_deleted` tinyint(3) UNSIGNED NOT NULL COMMENT 'Удалена' DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item`
--

CREATE TABLE `menu_item` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `menu_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `is_new_window` tinyint(1) UNSIGNED DEFAULT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `parent_id` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `root` tinyint(3) UNSIGNED DEFAULT NULL,
  `lft` tinyint(3) UNSIGNED DEFAULT NULL,
  `rgt` tinyint(3) UNSIGNED DEFAULT NULL,
  `depth` tinyint(3) UNSIGNED DEFAULT NULL,
  `nofollow` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_translation`
--

CREATE TABLE `menu_item_translation` (
  `menu_item_id` tinyint(3) UNSIGNED NOT NULL,
  `lang` varchar(255) NOT NULL COMMENT 'Язык перевода',
  `title` varchar(255) NOT NULL COMMENT 'Наименование',
  `link` varchar(255) NOT NULL COMMENT 'Route',
  `params` varchar(255) DEFAULT NULL COMMENT 'Параметры'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `menu_translation`
--

CREATE TABLE `menu_translation` (
  `menu_id` tinyint(3) UNSIGNED NOT NULL,
  `lang` varchar(255) NOT NULL COMMENT 'Язык перевода',
  `title` varchar(255) NOT NULL COMMENT 'Наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) UNSIGNED NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `image` varchar(255) DEFAULT NULL,
  `publish_at` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(3) UNSIGNED NOT NULL COMMENT 'Удалена' DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `news_translation`
--

CREATE TABLE `news_translation` (
  `news_id` int(11) UNSIGNED NOT NULL,
  `lang` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `short_text` text DEFAULT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(11) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Активация',
  `position` int(11) UNSIGNED DEFAULT NULL COMMENT 'Позиция',
  `additionl_fields` text DEFAULT NULL COMMENT 'Дполнительный поля в JSON',
  `created_at` datetime NOT NULL COMMENT 'Дата создания',
  `updated_at` datetime DEFAULT NULL COMMENT 'Дата редактирования',
  `image` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(3) UNSIGNED NOT NULL COMMENT 'Удалена' DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pages_translation`
--

CREATE TABLE `pages_translation` (
  `id` int(11) UNSIGNED NOT NULL,
  `page_id` int(11) UNSIGNED NOT NULL COMMENT 'Страница',
  `lang` varchar(20) NOT NULL COMMENT 'Язык перевода',
  `title` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `subtitle` varchar(255) DEFAULT NULL COMMENT 'Подзаголовок',
  `image` varchar(255) DEFAULT NULL COMMENT 'Картинка',
  `text` text DEFAULT NULL COMMENT 'Текст',
  `seo_text` text DEFAULT NULL COMMENT 'SEO Текст',
  `additionl_fields` text DEFAULT NULL COMMENT 'Дполнительный поля в JSON',
  `slug` varchar(255) DEFAULT NULL COMMENT 'ЧПУ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Структура таблицы `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) UNSIGNED NOT NULL,
  `section_id` int(11) UNSIGNED DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `finished_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `position` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `site_id` int(11) DEFAULT NULL COMMENT 'Сайт',
  `section_item_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(3) UNSIGNED NOT NULL COMMENT 'Удалена' DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sliders_items`
--

CREATE TABLE `sliders_items` (
  `id` int(11) UNSIGNED NOT NULL,
  `slider_id` int(11) UNSIGNED DEFAULT NULL,
  `thumb_desktop` varchar(255) DEFAULT NULL,
  `thumb_adaptive` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `position` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sliders_items_translation`
--

CREATE TABLE `sliders_items_translation` (
  `item_id` int(11) UNSIGNED NOT NULL,
  `lang` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT 'Описание'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sliders_translation`
--

CREATE TABLE `sliders_translation` (
  `slider_id` int(11) UNSIGNED NOT NULL,
  `lang` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Индексы таблицы `handbooks`
--
ALTER TABLE `handbooks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-position` (`position`),
  ADD KEY `idx-is_active` (`is_active`);

--
-- Индексы таблицы `handbooks_translation`
--
ALTER TABLE `handbooks_translation`
  ADD PRIMARY KEY (`handbook_id`,`lang`),
  ADD KEY `idx_title` (`title`),
  ADD KEY `idx_slug` (`slug`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active_menu` (`is_active`);

--
-- Индексы таблицы `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id_index` (`menu_id`),
  ADD KEY `new_window` (`is_new_window`),
  ADD KEY `active_index` (`is_active`),
  ADD KEY `parent_index` (`parent_id`),
  ADD KEY `root_index` (`root`),
  ADD KEY `lft_index` (`lft`),
  ADD KEY `rgt_index` (`rgt`),
  ADD KEY `depth_index` (`depth`),
  ADD KEY `nofollow` (`nofollow`);

--
-- Индексы таблицы `menu_item_translation`
--
ALTER TABLE `menu_item_translation`
  ADD PRIMARY KEY (`menu_item_id`,`lang`);

--
-- Индексы таблицы `menu_translation`
--
ALTER TABLE `menu_translation`
  ADD UNIQUE KEY `uq-menu_translation` (`menu_id`,`lang`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-is_active` (`is_active`),
  ADD KEY `idx-publish_at` (`publish_at`);

--
-- Индексы таблицы `news_translation`
--
ALTER TABLE `news_translation`
  ADD PRIMARY KEY (`news_id`,`lang`),
  ADD KEY `idx-title` (`title`),
  ADD KEY `idx-slug` (`slug`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-is_active` (`is_active`),
  ADD KEY `idx-position` (`position`);

--
-- Индексы таблицы `pages_translation`
--
ALTER TABLE `pages_translation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq-pages_translation-lang-page_id` (`lang`,`page_id`),
  ADD KEY `idx-page_id` (`page_id`),
  ADD KEY `idx-title` (`title`),
  ADD KEY `idx-slug` (`slug`);

--
-- Индексы таблицы `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK-sliders-user_id-user-id` (`user_id`),
  ADD KEY `idx-section_id` (`section_id`),
  ADD KEY `idx-position` (`position`);

--
-- Индексы таблицы `sliders_items`
--
ALTER TABLE `sliders_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK-sliders_items-user_id-user-id` (`user_id`),
  ADD KEY `FK-sliders-slider_id-sliders-id` (`slider_id`);

--
-- Индексы таблицы `sliders_items_translation`
--
ALTER TABLE `sliders_items_translation`
  ADD PRIMARY KEY (`item_id`,`lang`),
  ADD KEY `idx-title` (`title`),
  ADD KEY `idx-slug` (`slug`);

--
-- Индексы таблицы `sliders_translation`
--
ALTER TABLE `sliders_translation`
  ADD PRIMARY KEY (`slider_id`,`lang`),
  ADD KEY `idx-title` (`title`),
  ADD KEY `idx-slug` (`slug`);

--
-- AUTO_INCREMENT для таблицы `handbooks`
--
ALTER TABLE `handbooks`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `pages_translation`
--
ALTER TABLE `pages_translation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `sliders_items`
--
ALTER TABLE `sliders_items`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- Ограничения внешнего ключа таблицы `handbooks_translation`
--
ALTER TABLE `handbooks_translation`
  ADD CONSTRAINT `fk-handbooks_translation-handbook_id` FOREIGN KEY (`handbook_id`) REFERENCES `handbooks` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_menu_item_menu` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE SET NULL;

--
-- Ограничения внешнего ключа таблицы `menu_item_translation`
--
ALTER TABLE `menu_item_translation`
  ADD CONSTRAINT `fk-menu_items-menu_items_tranlation` FOREIGN KEY (`menu_item_id`) REFERENCES `menu_item` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `menu_translation`
--
ALTER TABLE `menu_translation`
  ADD CONSTRAINT `fk-menu-menu_tranlation` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `news_translation`
--
ALTER TABLE `news_translation`
  ADD CONSTRAINT `fk-news_translations-news` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `pages_translation`
--
ALTER TABLE `pages_translation`
  ADD CONSTRAINT `pages_translation-page_id` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `FK-sliders-section_id-handbooks-id` FOREIGN KEY (`section_id`) REFERENCES `handbooks` (`id`),
  ADD CONSTRAINT `FK-sliders-user_id-user-id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `sliders_items`
--
ALTER TABLE `sliders_items`
  ADD CONSTRAINT `FK-sliders-slider_id-sliders-id` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK-sliders_items-user_id-user-id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `sliders_items_translation`
--
ALTER TABLE `sliders_items_translation`
  ADD CONSTRAINT `fk-sliders_items_translation-item_id-sliders_item-id` FOREIGN KEY (`item_id`) REFERENCES `sliders_items` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sliders_translation`
--
ALTER TABLE `sliders_translation`
  ADD CONSTRAINT `fk-sliders_translation-slider_id-sliders-id` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
