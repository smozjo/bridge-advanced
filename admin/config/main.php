<?php

use admin\modules\user\models\search\UserSearch;
use Bridge\Core\BridgeModule;
use common\helpers\LanguageHelper;
use common\models\Profile;
use Da\User\Module;
use prokhonenkov\repeater\Repeater;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-admin',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'admin',
    'controllerNamespace' => 'admin\controllers',
    'bootstrap' => ['log', 'admin', 'repeater'],
    'modules' => [
        'admin' => [
            'class' => BridgeModule::class,
            'languages' => LanguageHelper::getLanguagesLabels(),
            // Add your projects modules here to keep right routing.
            'modules' => [
                'menu' => [
                    'class' => 'admin\modules\menu\Module',
                ],
                'handbooks' => [
                    'class' => 'admin\modules\handbooks\Module',
                ],
                'pages' => [
                    'class' => 'admin\modules\pages\Module',
                ],
                'sliders' => [
                    'class' => 'admin\modules\sliders\Module',
                ],
                'news' => [
                    'class' => 'admin\modules\news\Module',
                ],
            ],
            // Add menu item of your content management module to menu
            'menu' => [
                [
                    'title' => 'Content',
                    'url' => ['/admin/content/default/index'],
                    'active' => ['module' => 'content'],
                    'icon' => 'list'
                ]
            ],
            'userSettings' => [
                'class' => Module::class,
                'administratorPermissionName' => 'admin',
                'enableRegistration' => true,
                'classMap' => [
                    'Profile' => Profile::class,
                    'UserSearch' => UserSearch::class,
                ]
            ],
            // Alternatively you can define different menu items for different
            // roles. In that case it will override default menu items, such as
            // settings, users and dashboard
            'composeMenu' => function ($user, $roles, $authManager) {
                /**
                 * @var \yii\web\User $user
                 * @var \Da\User\Model\Role[] $roles
                 * @var \Da\User\Component\AuthDbManagerComponent $authManager
                 */
                if (isset($roles['admin'])) {
                    return require __DIR__ . '/menu-admin.php';
                }
                if ($user->can('editor')) {
                    return require __DIR__ . '/menu-editor.php';
                }
                if (in_array($user->id, $authManager->getUserIdsByRole('manager'))) {
                    return require __DIR__ . '/menu-manager.php';
                }
                // Or any other available method

                return __DIR__ . '/menu-default.php';
            },
            'on beforeAction' => function(\yii\base\ActionEvent $event) {
                $isDashBoard = $event->action->controller->module->id === 'admin' &&
                    $event->action->controller->id === 'default' &&
                    $event->action->id === 'index';

                if(!$isDashBoard) {
                    return null;
                }

                if(\Yii::$app->user->isGuest) {
                    return null;
                }

                if(\Yii::$app->user->getIdentity()->hasRole('admin')) {
                    header('Location: /admin/news/news');
                    exit;
                }
            },
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module'
        ],
        'repeater'=> [
            'class' => Repeater::class
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-admin',
        ],
        'user' => [
            'identityClass' => \Da\User\Model\User::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-admin', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the admin
            'name' => 'advanced-admin',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'admin/default/error',
        ],
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules'=> [
            ],
            'languages' => \common\helpers\LanguageHelper::getLanguages(),
            'enableDefaultLanguageUrlCode' => true,
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@Da/User/resources/views' => '@app/modules/user/views',
                ],
            ],
        ],
    ],
    'params' => $params,
];
