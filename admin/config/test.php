<?php
return [
    'id' => 'app-admin-tests',
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => [
            ],
            'languages' => \common\helpers\LanguageHelper::getLanguages(),
            'enableDefaultLanguageUrlCode' => false,
        ],
        'request' => [
            'cookieValidationKey' => 'test',
        ],
    ],
];
