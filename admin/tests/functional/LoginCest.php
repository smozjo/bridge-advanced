<?php

namespace admin\tests\functional;

use admin\tests\functional\base\BaseCest;
use admin\tests\FunctionalTester;
use common\fixtures\UserFixture;

/**
 * Class LoginCest
 */
class LoginCest extends BaseCest
{
    /**
     * @param FunctionalTester $I
     */
    public function loginUser(FunctionalTester $I)
    {
        $I->amOnPage('/admin/default/login');
        $I->fillField('Username', 'admin');
        $I->fillField('Password', '123456');
        $I->click('login-button');

        $I->see('Logout');
        $I->dontSeeLink('Forgot password?');
    }
}
