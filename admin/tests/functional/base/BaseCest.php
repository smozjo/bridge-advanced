<?php

namespace admin\tests\functional\base;

use common\fixtures\AuthAssignmentFixture;
use common\fixtures\AuthItemFixture;
use common\fixtures\UserFixture;

class BaseCest
{
    const USER_ADMIN = 1; // администратор сайта

    /**
     * Load fixtures before db transaction begin
     * Called in _before()
     * @see \Codeception\Module\Yii2::_before()
     * @see \Codeception\Module\Yii2::loadFixtures()
     * @return array
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php',
            ],
            'auth_item' => [
                'class' => AuthItemFixture::class,
                'dataFile' => codecept_data_dir() . 'auth_item.php',
            ],
            'auth_assigment' => [
                'class' => AuthAssignmentFixture::class,
                'dataFile' => codecept_data_dir() . 'auth_assigment.php',
            ],
        ];
    }
}
