<?php

namespace admin\modules\news\controllers;

use common\core\CoreAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;
use yii2tech\admin\actions\Restore;
use yii2tech\admin\actions\SoftDelete;

/**
 * NewsController implements the CRUD actions for [[common\models\news\News]] model.
 * @see common\models\news\News
 */
class NewsController extends CoreAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\news\News';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'common\models\news\search\NewsSearch';
    /**
     * @inheritdoc
     */
    public $allowedRoles = ['admin'];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'common\models\news\News',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'delete' => [
                    'class' => SoftDelete::class,
                ],
                'restore' => [
                    'class' => Restore::class,
                ],
                    'position' => [
                        'class' => Position::class,
                    ],
            ]
        );
    }
}
