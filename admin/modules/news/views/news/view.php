<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\news\News */

$this->title = 'Просмотр: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'is_active',
                'format' => 'raw',
                'value' => function($data){
                    return $data->is_active ? '<span class="text-success">Показывается</span>' : '<span class="text-danger">Не показывается</span>';
                }
            ],
            'publish_at',
            'title',
            [
                'attribute' => 'slug',
                'format' => 'raw',
                'value' => function($data){
                    return '<a href="' . \Yii::$app->urlManagerFront->createUrl(['/news/' . $data->slug]) . '" target="_blank">' . \Yii::$app->urlManagerFront->createUrl(['/news/' . $data->slug]) . '</a>';
                }
            ],
            [
                'attribute' => 'shortText',
                'format' => 'raw',
                'label' => 'Краткое описание',
            ],
            'text:html',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($model){
                    return '<img src="' . $model->getImageBig() . '">';
                }
            ],
            [
                'attribute' => 'cropper650x400',
                'format' => 'raw',
                'value' => function($model){
                    return '<img src="' . $model->getImageIndex() . '">';
                }
            ],
            [
                'attribute' => 'cropper422x200',
                'format' => 'raw',
                'value' => function($model){
                    return '<img src="' . $model->getImagelist() . '">';
                }
            ],
        ],
    ]) ?>
    </div>
</div>