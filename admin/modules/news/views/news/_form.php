<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\news\News */
/* @var $form Bridge\Core\Widgets\ActiveForm */

$this->attachBehavior('CkePageBehavior', [
    'class' => \common\components\ckepage\CkePageBehavior::class,
    'form' => 'form',
    'model' => $model,
    'editorId' => ['newstranslation-{{lang}}-text', 'newstranslation-{{lang}}-short_text'],
    'editableContentContainer' => '.content',
    'plugin' => 'kaztube-tools'
])

?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->translate($model, '@app/modules/news/views/news/_form-translate') ?>

        <?= $form->field($model, 'is_active')->switchInput()->label(
            \Yii::$app->user->getIdentity()->hasRole('correspondent')
                ? \Yii::t('app', 'На модерацию')
                : $model->getAttributeLabel('is_active')
        ) ?>

        <?= $form->metaTags($model->metaTag, '@vendor/rocket-php-lab/yii2-bridge-core/src/views/meta-page/_form-meta-tags') ?>

    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'publish_at')->dateTimePicker() ?>

        <?= \common\components\cropper\widget\CropperWIdget::widget([
            'id' => 'cropper-1920x743',
            'prefix' => '940x460',
            'form' => $form,
            'model' => $model,
            'attribute' => 'image',
            'height' => '100',
            'cropWidth' => '940',
            'cropHeight' => '460'
        ]) ?>

        <?= \common\components\cropper\widget\CropperWIdget::widget([
            'id' => 'cropper-650x400',
            'prefix' => '650x400',
            'form' => $form,
            'model' => $model,
            'attribute' => 'cropper650x400',
            'height' => '100',
            'cropWidth' => '650',
            'cropHeight' => '400'
        ]) ?>

        <?= \common\components\cropper\widget\CropperWIdget::widget([
            'id' => 'cropper-422x200',
            'prefix' => '422x200',
            'form' => $form,
            'model' => $model,
            'attribute' => 'cropper422x200',
            'height' => '100',
            'cropWidth' => '422',
            'cropHeight' => '200'
        ]) ?>

    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
