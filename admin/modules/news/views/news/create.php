<?php

/* @var $this yii\web\View */
/* @var $model common\models\news\News */

$this->title = 'Добавление';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>