<?php

use dosamigos\grid\GridView;
use yii\helpers\Html;
use yii2tech\admin\grid\ActionColumn;
use common\models\news\News;

/* @var $this yii\web\View */
/* @var $searchModel common\models\news\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    [
        'url' => ['index', Html::getInputName($searchModel, 'is_deleted') => !$searchModel->is_deleted],
        'label' => $searchModel->is_deleted ? \Yii::t('bridge', 'All records') : \Yii::t('bridge', 'Trash'),
        'icon' => $searchModel->is_deleted ? 'share-alt' : 'trash',
        'class' => 'btn btn-' . ($searchModel->is_deleted ? 'soft-info' : 'trash'),
    ],
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'searchTitle',
            'value' => function(News $model){
                return $model->getTitle();
            }
        ],
        [
            'attribute' => 'searchShortText',
            'value' => function(News $model){
                return strip_tags($model->getShortText());
            },
        ],
        [
            'attribute' => 'isModeration',
            'value' => function(News $model) use($searchModel): ?string {
                if($model->publish_at && $model->is_active) {
                    return \Yii::t('app', $searchModel::$workStatuses[$searchModel::IS_PUBLISH]);
                } elseif(!$model->publish_at && !$model->is_active) {
                    return \Yii::t('app', $searchModel::$workStatuses[$searchModel::ON_WORK]);
                } elseif(!$model->publish_at && $model->is_active) {
                    return \Yii::t('app', $searchModel::$workStatuses[$searchModel::WAIT_MODERATION]);
                } elseif($model->publish_at && !$model->is_active) {
                    return \Yii::t('app', $searchModel::$workStatuses[$searchModel::IN_ACTIVE]);
                }

                return null;
            },
            'filter' => array_map(function (string $item) {
            	return \Yii::t('app', $item);
			}, $searchModel::$workStatuses),
            'visible' => !\Yii::$app->user->getIdentity()->hasRole('correspondent'),
        ],
        [
            'attribute' => 'publish_at',
            'visible' => !\Yii::$app->user->getIdentity()->hasRole('correspondent'),
        ],
        [
            'class' => \common\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
            'header' => \Yii::$app->user->getIdentity()->hasRole('correspondent')
                ? \Yii::t('app', 'На модерации')
                : $searchModel->getAttributeLabel('is_active'),
            'filter' => ['Нет', 'Да']
        ],
        [
            'class' => \common\components\admin\RFAActionColumn::class,
            'template' => '{view}{update}{delete}'
        ],
    ],
]); ?>
