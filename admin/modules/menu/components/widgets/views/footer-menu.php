<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 18.10.2019
 * Time 14:17
 */

use admin\modules\menu\components\MenuConfigurator;
use common\models\menu\Menu;
use yii\helpers\Url;

$menu = MenuConfigurator::getInstance()->getById(Menu::MENU_FOOTER);
$items = [];

if($menu) {
	$items = $menu->getItems();
}


?>
<?php foreach ($items as $item): ?>
    <div class="footer-menu">
        <h3 class="footer-menu__title"><?= $item['label']; ?></h3>
        <nav class="footer-menu__links">
            <?php if(isset($item['items'])): ?>
                <?php foreach ($item['items'] as $subItem): ?>
                    <a
                        href="<?= Url::to($subItem['url']) ?>"
                        class="footer-menu__link"
                        <?= $subItem['isNewWindow'] ? 'target="_blank"' : ''?>
                        <?= $subItem['isNofollow'] ? 'rel="nofollow"' : ''?>
                    ><?= $subItem['label']; ?></a>
                <?php endforeach; ?>
            <?php endif; ?>
        </nav>
    </div>
<?php endforeach; ?>