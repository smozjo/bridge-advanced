<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 11.11.2019
 * Time 9:25
 */

use admin\modules\menu\components\MenuConfigurator;
use common\models\menu\Menu;
use yii\helpers\Url;

$menu = MenuConfigurator::getInstance()->getById(Menu::MENU_MAIN);

if(!$menu) {
    $items = [];
} else {
    $items = $menu->getItems();
}
?>


<ul class="header__menu">
	<?php foreach ($items as $item): ?>
		<li class="header__menu--item <?= $item['image'] ? 'header__menu-img' : ''?> <?= Url::to($item['url']) != $activeMenuUrl ?: 'item-active'?>">
			<a
				href="<?= Url::to($item['url']) ?>"
				<?= $item['isNewWindow'] ? 'target="_blank"' : ''?>
				<?= $item['isNofollow'] ? 'rel="nofollow"' : ''?>
			>
				<?= $item['label'];?>
                <?php if($item['image']): ?>
                    <img class="header__menu-img__image" src="<?= $item['image'];?>" alt=""/>
                <?php endif; ?>
			</a>
		</li>
	<?php endforeach; ?>
</ul>