<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 11.11.2019
 * Time 9:25
 */

use admin\modules\menu\components\MenuConfigurator;
use common\models\menu\Menu;
use yii\helpers\Url;

$menu = MenuConfigurator::getInstance()->getById(Menu::MENU_PROFILE_HEADER);
$user = \Yii::$app->getUser()->getIdentity();
if(!$user) return false;
$userName = $user->username;

if(!$menu) {
    $items = [];
} else {
    $items = $menu->getItems();
}
?>
<ul class="header__right-block header__right-block__account-dropdown">
    <?php foreach ($items as $item): ?>
        <a class="header__right-block__account-dropdown__item" href="<?= Url::to(str_replace('{userName}',$userName,$item['url'])) ?>"
            <?= $item['isNewWindow'] ? 'target="_blank"' : ''?>
            <?= $item['isNofollow'] ? 'rel="nofollow"' : ''?>
        >
            <?php if($item['image']): ?><img src="<?= $item['image'] ?>" alt="<?= $item['label'];?>"/><?php endif; ?>
            <?= $item['label'];?>
        </a>
    <?php endforeach; ?>
    <li class="header__right-block__account-dropdown__item account--exit"><a href="#"><?= \Yii::t('app', 'Выйти из аккаунта')?></a></li>
</ul>