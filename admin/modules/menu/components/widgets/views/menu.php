<?php
/**
 * @var $menuId int
 */

use admin\modules\menu\components\MenuConfigurator;
use yii\helpers\ArrayHelper;

$menu = MenuConfigurator::getInstance()->getById($menuId);

$items = [];

if($menu) {
	$items = $menu->getItems();
}
echo \app\modules\menu\components\widgets\Menu::widget([
    'items' => $items,
    'options' => ArrayHelper::merge([
		'class' => 'footer-menu',
	], $options),
	'itemOptions' => ArrayHelper::merge([
		'class' => 'footer-menu__item',
	], $itemOptions),
    'linkTemplate' => '<a href="{url}" {nofollow} {target} class="' . $linkClass . '" data-hover="{label}">{label}</a>',
    'route' => Yii::$app->request->url,
    'activeCssClass' => 'active'
]);
