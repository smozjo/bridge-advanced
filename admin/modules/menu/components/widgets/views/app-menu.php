<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 18.10.2019
 * Time 14:17
 */

use admin\modules\menu\components\MenuConfigurator;
use common\models\menu\Menu;
use yii\helpers\Url;

$items = MenuConfigurator::getInstance()->getById(Menu::MENU_APPS)->getItems();


?>
<nav class="links">
	<?php foreach ($items as $item): ?>
        <a href="<?= Url::to($item['url']) ?>">
            <img data-src="<?= $item['image']; ?>" class="lazyload" alt=""/>
        </a>
	<?php endforeach; ?>
</nav>