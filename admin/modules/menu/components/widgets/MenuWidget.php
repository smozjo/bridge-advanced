<?php

namespace admin\modules\menu\components\widgets;

use admin\modules\menu\components\MenuConfigurator;
use yii\base\Widget;
use yii\helpers\Url;

class MenuWidget extends Widget
{

    public $menuId;
    public $viewFile = 'menu';
    public $id;
    public $isMobile = false;
    public $options = [];
    public $itemOptions = [];
    public $linkClass = 'footer-menu__link';

    public function run()
    {
        $activeMenuUrl = Url::current();
        if (!empty(\Yii::$app->controller->activeMenuUrl)) {
            $activeMenuUrl = \Yii::$app->controller->activeMenuUrl;

            if (is_array($activeMenuUrl)) {
                $activeMenuUrl = Url::to($activeMenuUrl);
            }
        }

        return $this->render($this->viewFile,[
        	'menuId' => $this->menuId,
			'id' => $this->id,
			'activeMenuUrl' => $activeMenuUrl,
			'isMobile' => $this->isMobile,
			'options' => $this->options,
			'itemOptions' => $this->itemOptions,
			'linkClass' => $this->linkClass
		]);
    }
}
