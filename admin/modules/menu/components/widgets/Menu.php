<?php
/**
 * Created by PhpStorm.
 * User: yevgeniy
 * Date: 2/23/15
 * Time: 3:04 PM
 */

namespace admin\modules\menu\components\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class Menu extends \yii\widgets\Menu
{
    protected function isItemActive($item)
    {
        if (!empty($this->route) && array_key_exists('url',
                $item) && is_array($item['url']) && isset($item['url'][0])
        ) {
            if (Url::to($item['url']) === $this->route) {
                return true;
            } else {
                return parent::isItemActive($item);
            }
        }

        return false;
    }

	protected function renderItem($item)
	{
		if (isset($item['url'])) {
			$template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

			return strtr($template, [
				'{url}' => Html::encode(Url::to($item['url'])),
				'{label}' => $item['label'],
				'{nofollow}' => $item['isNofollow'] ? 'rel="nofollow"' : '',
				'{target}' => $item['isNewWindow'] ? 'target="_blank"' : '',
			]);
		}

		$template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

		return strtr($template, [
			'{label}' => $item['label'],
		]);
	}
}
