<?php
/**
 * Created by PhpStorm.
 * User: prokhonenkov
 * Date: 15.03.19
 * Time: 16:48
 */

namespace admin\modules\menu\components\behaviors;


use admin\modules\menu\components\MenuConfigurator;
use admin\modules\settings\components\SettingsConfigurator;
use yii\base\Behavior;
use yii\web\Controller;
use yii\web\View;

class MenuBehavior extends Behavior
{
	public function events()
	{
		if(\Yii::$app->request->isAjax) {
			return [];
		}
		return [
			Controller::EVENT_BEFORE_ACTION => 'beforeAction',
		];
	}

	public function beforeAction()
	{

		$this->owner->getView()->on(View::EVENT_BEFORE_RENDER, function () {
			MenuConfigurator::getInstance()->execute();
		});
	}
}