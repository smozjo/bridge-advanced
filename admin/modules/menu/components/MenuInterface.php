<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 03.04.19
 * Time 10:52
 */

namespace admin\modules\menu\components;


interface MenuInterface
{
	public function getItems(): array ;
}