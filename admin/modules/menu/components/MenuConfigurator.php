<?php


namespace admin\modules\menu\components;


use common\models\menu\Menu;

class MenuConfigurator
{
	/**
	 * @var self
	 */
	private static $instance = null;

	private $menus = [];

	private $menuIds = [];

	/**
	 * @var bool
	 */
	private $isExecute = false;

	private function __clone() {}
	private function __construct() {}

	public static function getInstance(): self
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function setMenuId(int $menuId): self
	{
		$this->menuIds[] = $menuId;
		return $this;
	}


	public function execute(): bool
	{
		if($this->isExecute) {
			return false;
		}

		$this->isExecute = true;

		$this->menus = Menu::find()
			->joinWith(['menuItems' => function($query){
				$query
					->joinWith('currentTranslation')
					->where([
						'menu_item.is_active' => 1,
					])
					->orderBy([
						'menu_item.root' => SORT_ASC,
						'menu_item.lft' => SORT_ASC
					]);
			}])
			->andWhere(['menu.id' => $this->menuIds])
			->indexBy('id')
			->all();

		return true;

	}

	public function getById(int $menuId): ?MenuInterface
	{
		return $this->menus[$menuId]??null;
	}

}