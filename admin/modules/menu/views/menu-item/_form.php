<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\menu\MenuItem */
/* @var $form Bridge\Core\Widgets\ActiveForm */

$this->registerCss('
    .tab-content {
        border-right: 1px solid;
        border-left: 1px solid;
        padding: 10px;
        border-bottom: 1px solid;
    }
    .nav-tabs {
        border: 1px solid;
    }
');

$this->registerJs(
<<<JS
    $("#menuitem-menu_id").change(function(){
         $.pjax.reload({
            container: '#parentList', 
            type:'POST', 
            data:{menu_id:this.value
         }});
    });
JS

)
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

		<?= $form->field($model, 'menu_id')->relationalDropDown(\common\models\menu\Menu::class, 'id', 'translation.title', [
			'options' => [
				'placeholder' => 'Выберите ...',
				'disabled' => false
			],
			'pluginOptions' => [
				/** Расскоментим, если понадобится выборка множество категории */
//                'multiple' => true,
				'allowClear' => true
			]
		]) ?>

		<?php \yii\widgets\Pjax::begin(['id' => 'parentList']); ?>

		<?= $form->field($model, 'parent_id')->relationalDropDown(\common\models\menu\MenuItem::class, 'id', 'translation.title', [
			'options' => [
				'placeholder' => 'Выберите ...',
				'disabled' => false
			],
			'pluginOptions' => [
				/** Расскоментим, если понадобится выборка множество категории */
//                'multiple' => true,
				'allowClear' => true
			]
		]) ?>

		<?php \yii\widgets\Pjax::end(); ?>

		<?= $form->translate($model, '@app/modules/menu/views/menu-item/_form-translate') ?>



    </div>

    <div class="col-md-4">
		<?= $form->field($model, 'type')->dropDownList(\common\models\menu\Menu::$linksTitles, ['prompt' => 'Выберите']) ?>

		<?= $form->field($model, 'image')->imageUpload([
			'pluginOptions' => [
				'deleteUrl' => \yii\helpers\Url::to([
					'/admin/menu/menu-item/delete-file',
					'id' => $model->id,
					'modelName' => $model::className(),
					'behaviorName' => 'imageUpload'
				])
			]
		]) ?>


		<?= $form->field($model, 'is_new_window')->switchInput() ?>

		<?= $form->field($model, 'nofollow')->switchInput() ?>
		<?= $form->field($model, 'is_active')->switchInput() ?>


    </div>
</div>

    <div class="form-group">
        <br>
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>

<?php
$types = json_encode(\common\models\menu\Menu::$links);
$this->registerJS(<<<JS
    var types = {$types};
    $('#menuitem-type').on('change', function(){
        $('.m-link').val(types[this.value] == undefined ? '': types[this.value].route);
        $('.m-params').val(types[this.value] == undefined ? '': types[this.value].params);
    })

JS
)


?>
