<?php
/**
 * @var $languageCode string
 * @var $model app\modules\post\models\Post
 * @var $translationModel app\modules\post\models\PostTranslation
 * @var $form \Bridge\Core\Widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);
?>

<?= $form->field($translationModel, '['.$languageCode.']menu_item_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '['.$languageCode.']lang')->hiddenInput()->label(false)  ?>
<?= $form->field($translationModel, '['.$languageCode.']title')->textInput() ?>
<?= $form->field($translationModel, '['.$languageCode.']link')->textInput(['class' => 'form-control m-link']) ?>
<?= $form->field($translationModel, '['.$languageCode.']params')->textInput(['class' => 'form-control m-params']) ?>