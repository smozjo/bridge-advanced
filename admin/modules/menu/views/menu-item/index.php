<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\menu\models\search\MenuItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пункты меню';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php $this->beginContent('@app/modules/menu/views/common/layout.php', ['isMenu' => true]) ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],


        [
            'attribute' => 'menu_id',
            'value' => function($data) {

                return $data->menu->translation->title;
            },
            'filter' => \common\models\menu\Menu::getDropdownList('title', 'id', [
                'join' => ['currentTranslation']
            ])
        ],
        [
            'class' => \common\components\grid\TreeColumn::class,
            'attribute' => 'title',
            'value' => function($data) {
                return $data->title;
            }
        ],
        [
            'class' => \common\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],
        [
            'value' => function ($data) {
                return \yii\helpers\Html::a('<i class="glyphicon glyphicon-arrow-up"></i>',
                        ['move', 'id' => $data->id, 'type' => 'up'], ['class' => 'btn btn-info'])
                    . '&nbsp;&nbsp;' . \yii\helpers\Html::a('<i class="glyphicon glyphicon-arrow-down"></i>',
                        ['move', 'id' => $data->id, 'type' => 'down'], ['class' => 'btn btn-info']);
            },
            'format' => 'raw',
            'options' => array('align' => 'center')
        ],
        // 'created_at',
        // 'updated_at',
        // 'parent_id',
        // 'root',
        // 'lft',
        // 'rgt',
        // 'level',
        // 'nofollow',

        ['class' => \common\components\admin\RFAActionColumn::class],
    ],
]); ?>

<?php $this->endContent() ?>