<?php

/* @var $this yii\web\View */
/* @var $model common\models\menu\MenuItem */

$this->title = 'Добавление';
$this->params['breadcrumbs'][] = ['label' => 'Пункты меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/modules/menu/views/common/layout.php', ['isMenu' => false]) ?>
<?= $this->render('_form', [
	'model' => $model,
]) ?>
<?php $this->endContent() ?>

