<?php
/**
 * @var $languageCode string
 * @var $model common\models\menu\Menu
 * @var $translationModel app\modules\post\models\PostTranslation
 * @var $form \Bridge\Core\Widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);

?>

<?= $form->field($translationModel, '['.$languageCode.']menu_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '['.$languageCode.']lang')->hiddenInput()->label(false)  ?>
<?= $form->field($translationModel, '['.$languageCode.']title')->textInput() ?>