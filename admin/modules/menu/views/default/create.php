<?php

/* @var $this yii\web\View */
/* @var $model common\models\menu\Menu */

$this->title = 'Создание меню';
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/modules/menu/views/common/layout.php', ['isMenu' => false]) ?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
<?php $this->endContent() ?>
