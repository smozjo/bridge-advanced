<?php

/* @var $this yii\web\View */
/* @var $model common\models\menu\Menu */

$this->title = 'Редактирование меню ';
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->translation->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<?php $this->beginContent('@app/modules/menu/views/common/layout.php', ['isMenu' => false]) ?>
<?= $this->render('_form', [
	'model' => $model,
]) ?>
<?php $this->endContent() ?>


