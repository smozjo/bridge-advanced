<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\menu\Menu */
/* @var $form Bridge\Core\Widgets\ActiveForm */


?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->translate($model, '@app/modules/menu/views/default/_form-translate') ?>
        <?= $form->field($model, 'is_active')->switchInput() ?>


    </div>

    <div class="col-md-4">

    </div>
</div>

    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
