<?php

use dosamigos\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\menu\search\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    [
        'url' => ['index', Html::getInputName($searchModel, 'is_deleted') => !$searchModel->is_deleted],
        'label' => $searchModel->is_deleted ? \Yii::t('bridge', 'All records') : \Yii::t('bridge', 'Trash'),
        'icon' => $searchModel->is_deleted ? 'share-alt' : 'trash',
        'class' => 'btn btn-' . ($searchModel->is_deleted ? 'soft-info' : 'trash'),
    ],
];
?>
<?php $this->beginContent('@app/modules/menu/views/common/layout.php', ['isMenu' => true]) ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'title',
            'value' => 'translation.title',
            'label' => Yii::t('app', 'Заголовок'),
        ],
        [
            'class' => \common\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],
        [
            'class' => \common\components\admin\RFAActionColumn::class
        ],
    ],
]); ?>

<?php $this->endContent() ?>
