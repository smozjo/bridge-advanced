<?php

use yii\bootstrap\Nav;

?>

<?= Nav::widget(
    [
        'options' => [
            'class' => 'nav-tabs',
            'style' => 'margin-bottom: 15px',
        ],
        'items' => [
            [
                'label' => Yii::t('app', 'Меню'),
                'url' => ['/admin/menu/default/index'],
            ],
            [
                'label' => Yii::t('app', 'Пункты меню'),
                'url' => ['/admin/menu/menu-item/index'],
            ],
            [
                'label' => Yii::t('app', 'Содать'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Меню'),
                        'url' => ['/admin/menu/default/create'],
                    ],
                    [
                        'label' => Yii::t('app', 'Пункт меню'),
                        'url' => ['/admin/menu/menu-item/create'],
                    ],
                ],
            ],
        ],
    ]
) ?>
