<?php

namespace admin\modules\menu\controllers;

use common\core\CoreAdminController;
use common\models\menu\MenuItem;
use \Yii;
use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * MenuItemController implements the CRUD actions for [[common\models\menu\MenuItem]] model.
 * @see common\models\menu\MenuItem
 */
class MenuItemController extends CoreAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\menu\MenuItem';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'common\models\menu\search\MenuItemSearch';
    /**
     * @inheritdoc
     */
    public $allowedRoles = ['admin'];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        return ArrayHelper::merge(
            $actions,
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'common\models\menu\MenuItem',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }

    public function actionCreate()
    {
        $model = new MenuItem();

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Если родитель не выбран, то делаем категорию корневой
             */
            if (!$model->parent_id) {
                $model->parent_id = 0;
                if ($model->makeRoot()) {
                    return $this->redirect(['index']);
                }
            } else {
                /**
                 * Если найден, то добавлем его к дереву
                 */
                $rootCategory = MenuItem::findOne($model->parent_id);
                if ($rootCategory === null) {
                    $model->addError('parent_id', 'Родительская категория не найдена');
                } else {
                    if ($model->appendTo($rootCategory)) {
                        return $this->redirect(['index']);
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MenuItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldParentId = $model->parent_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if ($oldParentId != (int)$model->parent_id && $model->parent_id != 0) {
                $parentCategory = MenuItem::findOne($model->parent_id);
                $model->appendTo($parentCategory);
            } else {
                if ($oldParentId != (int)$model->parent_id && $model->parent_id == 0) {
                    $model->makeRoot();
                }
            }

            return $this->redirect(['index']);


        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->isRoot() ? $model->deleteWithChildren() : $model->delete();

        return $this->redirect(['index']);
    }

    public function actionMove($id, $type)
    {
        /**
         * @var \yii\db\ActiveRecord | \creocoder\nestedsets\NestedSetsBehavior $model
         */
        $model = MenuItem::findOne((int)$id);

        if ($model === null) {
            throw new NotFoundHttpException(404, 'Страница не найдена');
        }

        if (!$model->isRoot()) {
            if ($type == 'up') {
                $prev = $model->prev()->one();
                if ($prev !== null) {
                    $model->insertBefore($prev);
                }
            } else {
                $next = $model->next()->one();
                if ($next !== null) {
                    $model->insertAfter($next);
                }
            }
        } else {
            $curPos = $model->root;
            if ($type == 'up') {
                $prevCategory = MenuItem::find()->where(
                    [
                        'menu_id' => $model->menu_id,
                        'depth' => 0
                    ])
                    ->andWhere('root<:root', [':root' => $model->root])
                    ->addOrderBy(['root' => SORT_DESC])->one();

                if ($prevCategory !== null) {
                    $newPos = $prevCategory->root;

                    $allDescendantsCurModel = $model->leaves()->all();
                    $allDescendantsPrevModel = $prevCategory->leaves()->all();

                    foreach ($allDescendantsCurModel as $curItem) {
                        $curItem->root = $newPos;
                        $curItem->save(false);
                    }

                    $model->root = $newPos;
                    $model->save(false);

                    foreach ($allDescendantsPrevModel as $prevItem) {
                        $prevItem->root = $curPos;
                        $prevItem->save();
                    }

                    $prevCategory->root = $curPos;
                    $prevCategory->save();
                }

            } else {
                $nextCategory = MenuItem::find()->where(
                    [
                        'menu_id' => $model->menu_id,
                        'depth' => 0
                    ])
                    ->andWhere('root>:root', [':root' => $model->root])
                    ->addOrderBy(['root' => SORT_ASC])->one();

                if ($nextCategory !== null) {
                    $newPos = $nextCategory->root;

                    $allDescendantsCurModel = $model->leaves()->all();
                    $allDescendantsNextModel = $nextCategory->leaves()->all();


                    foreach ($allDescendantsCurModel as $curItem) {
                        $curItem->root = $newPos;
                        $curItem->save(false);
                    }

                    $model->root = $newPos;
                    $model->save();

                    foreach ($allDescendantsNextModel as $prevItem) {
                        $prevItem->root = $curPos;
                        $prevItem->save(false);
                    }

                    $nextCategory->root = $curPos;
                    $nextCategory->save(false);
                }

            }
        }
        $this->redirect(array('index'));
    }
}
