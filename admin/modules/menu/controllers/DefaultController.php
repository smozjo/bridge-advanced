<?php

namespace admin\modules\menu\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;
use yii2tech\admin\actions\Restore;
use yii2tech\admin\actions\SoftDelete;

/**
 * DefaultController implements the CRUD actions for [[common\models\menu\Menu]] model.
 * @see common\models\menu\Menu
 */
class DefaultController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\menu\Menu';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'common\models\menu\search\MenuSearch';
    /**
     * @inheritdoc
     */
    public $allowedRoles = ['admin'];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'common\models\menu\Menu',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'delete' => [
                    'class' => SoftDelete::class,
                ],
                'restore' => [
                    'class' => Restore::class,
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
