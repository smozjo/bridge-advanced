<?php

/* @var $this yii\web\View */
/* @var $model common\models\sliders\Sliders */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

