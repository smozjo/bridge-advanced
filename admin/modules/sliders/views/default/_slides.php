<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 07.10.2019
 * Time 16:44
 */

use \common\models\sliders\SlidersItems;
use yii\helpers\Html;
use \common\components\manageRelation\widgets\ManageRelationWidget;

/** @var \common\models\sliders\Sliders $model */



?>

<?php $this->beginContent('@app/views/layouts/admin/_panel.php', ['title' => 'Слайды']) ?>


<?= ManageRelationWidget::widget([
	'dataProviderQuery' => $model->getSlidersItems()->positionAsc(),
	'prevAction' => '/admin/sliders/sliders-items/position',
	'nextAction' => '/admin/sliders/sliders-items/position',
	'toggleAction' => '/admin/sliders/sliders-items/toggle',
	'deleteAction' => '/admin/sliders/sliders-items/remove',
	'form' => $form,
	'className' => \common\models\sliders\SlidersItems::class,
	'repeaterView' => '@app/modules/sliders/views/default/_repeater-item',
	'repeaterId' => 'slidersItemsRepeater',
	'repeaterBtnTitle' => 'Добавить слайд',
	'tableId' => 'sliders-items',
	'gridColumns' => [
		'title',
		'position',
		'is_active',
//		[
//			'class' => 'yii2tech\admin\grid\PositionColumn',
//			'value' => 'position',
//			'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
//			'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
//			'route' => '/admin/sliders/sliders-items/position'
//		],
		[
			'class' => \common\components\admin\RFAToggleColumn::class,
			'attribute' => 'is_active',
			'url' => ['items', 'type' => \Yii::$app->request->get('type')]
		],
	]
]);?>

<?php $this->endContent() ?>
