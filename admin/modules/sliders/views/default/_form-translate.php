<?php
/**
 * @var $languageCode string
 * @var $model \admin\modules\persons\models\Persons
 * @var $translationModel \admin\modules\persons\models\translation\PersonsTranslation
 * @var $form \Bridge\Core\Widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);
?>

<?= $form->field($translationModel, '['.$languageCode.']slider_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '['.$languageCode.']lang')->hiddenInput()->label(false)  ?>
<?= $form->field($translationModel, '['.$languageCode.']title')->textInput(['disabled' => false]) ?>