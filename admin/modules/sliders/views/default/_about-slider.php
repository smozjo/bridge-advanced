<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 07.10.2019
 * Time 16:44
 */

use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\sliders\Sliders */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $this->beginContent('@app/views/layouts/admin/_panel.php', ['title' => 'Слайдер']) ?>

	<div class="col-md-8">

		<?= $form->translate($model, '@app/modules/sliders/views/default/_form-translate') ?>

        <?= $form->field($model, 'section_id')->dropDownList(\common\models\handbooks\Sections::getDropdownList('title', 'id', ['methods' => [
            ['currentTranslation']
        ]])) ?>

    </div>

	<div class="col-md-4">
		<?php $this->beginContent('@app/views/layouts/admin/_panel.php', ['title' => 'Настройки']) ?>
		<?= $form->field($model, 'finished_at')->dateTimePicker() ?>

		<?= $form->field($model, 'is_active')->switchInput() ?>

		<?php $this->endContent() ?>
	</div>


<?php $this->endContent() ?>