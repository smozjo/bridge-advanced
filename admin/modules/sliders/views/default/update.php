<?php

/* @var $this yii\web\View */
/* @var $model common\models\sliders\Sliders */

$this->title = 'Редактировать: ' . $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


