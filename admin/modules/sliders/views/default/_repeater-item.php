<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 02.10.2019
 * Time 9:26
 */

use kartik\widgets\SwitchInput;
use yii\bootstrap\Tabs;
use common\models\sliders\SlidersItems;
use kartik\file\FileInput;
use yii\helpers\Json;

/** @var \common\models\sliders\SlidersItems $model */
/** @var int $id */
/** @var \yii\web\View $this */
/** @var int $additionalClientData */

$form = \Yii::$app->request->isAjax ? new \Bridge\Core\Widgets\ActiveForm() : $form;

$items = [];
$active = true;

if($additionalClientData) {
    $model = SlidersItems::findOne($additionalClientData);
}


foreach (\Yii::$app->getModule('admin')->languages as $languageCode => $language) {
	$translationModel = $model->getTranslation($languageCode);
	$items[] = [
		'label' => $language,
		'active' => $active,
		'content' => $form->field(
				$translationModel,
			"title"
		)->textInput([
		        'name' => "SlidersItemsRepeater[$id][SlidersItemsTranslation][$languageCode][title]"
        ]) .
            $form->field($translationModel, "description")->textInput([
                'name' => "SlidersItemsRepeater[$id][SlidersItemsTranslation][$languageCode][description]"
            ]) .
            $form->field($translationModel, "slug")->textInput([
                'name' => "SlidersItemsRepeater[$id][SlidersItemsTranslation][$languageCode][slug]"
            ]) .
            $form->field($translationModel, "lang")->hiddenInput([
				'name' => "SlidersItemsRepeater[$id][SlidersItemsTranslation][$languageCode][lang]"
            ])->label(false) .
            $form->field($translationModel, "item_id")->hiddenInput([
				'name' => "SlidersItemsRepeater[$id][SlidersItemsTranslation][$languageCode][item_id]"
            ])->label(false)
	];

	$active = false;
}
$fileInputPluginOptionsDesctop = $fileInputPluginOptionsAdaptive = [
    "language" => \Yii::$app->language,
    "resizeImage" => false,
    "autoOrientImage"=>true,
    "purifyHtml"=>true,
	'showUpload' => false,
	'showRemove' => false,
	'initialPreviewAsData'=>true,
];

$fileInputPluginOptionsDesctop['initialPreview'] = [$model->getUploadUrl('thumb_desktop')];
$fileInputPluginOptionsAdaptive['initialPreview'] = [$model->getUploadUrl('thumb_adaptive')];


$fileInputOptionDesctopJson = Json::htmlEncode($fileInputPluginOptionsDesctop);
$fileInputOptionDesctopHash = hash('crc32', $fileInputOptionDesctopJson);

$fileInputOptionAdaptiveJson = Json::htmlEncode($fileInputPluginOptionsAdaptive);
$fileInputOptionAdaptiveHash = hash('crc32', $fileInputOptionAdaptiveJson);


$pluginOptionJson = \yii\helpers\Json::htmlEncode($pluginOptions);
$pluginOptionHash = hash('crc32', $pluginOptionJson);

$this->registerJs(
<<<JS
    if($id != 0) {
        window.location.hash = '#sliders-item-$id';
    }
    
    window.select2_{$pluginOptionHash} = {$pluginOptionJson}; 
    window.fileinput_{$fileInputOptionDesctopHash} = {$fileInputOptionDesctopJson};
    window.fileinput_{$fileInputOptionAdaptiveHash} = {$fileInputOptionAdaptiveJson};
JS
);


//print_r($fileInputPluginOptionsDesctop);exit;
?>

<?php $this->beginContent('@app/views/layouts/admin/_panel.php', [
        'title' => $model->id
            ? sprintf('%s "%s"',
				\Yii::t('app', 'Слайд'),
              //  $model->position,
                $model->getTitle()
            )
            : \Yii::t('app', 'Новый слайд')
]) ?>

    <table width="100%" id="sliders-item-<?=$id?>">
        <tbody>
        <tr>
            <td style="padding-right: 20px; " width="50%">
				<?= Tabs::widget([
					'id' => 'title-tabs-' . $id,
					'items' => $items
				])
				?>
				<?=   $form->field($model, "id")->hiddenInput([
					'name' => "SlidersItemsRepeater[$id][SlidersItems][id]"
				])->label(false)?>


                <div class="form-group">
                    <label><?= $model->getAttributeLabel('is_active')?></label>
					<?= SwitchInput::widget([
						'model' => $model,
						'attribute' => "[$id]is_active",
						'options' => [
							'name' => "SlidersItemsRepeater[$id][SlidersItems][is_active]"
						]
					])?>
                </div>

            </td>
            <td>
                <label><?= \Yii::t('app', 'Изображение (десктоп)')?></label>
				<?= FileInput::widget([
					'model' => $model,
					'pluginOptions' => $fileInputPluginOptionsDesctop,
					'attribute' => "[$id]thumb_desktop",
					'options' => ['multiple' => false],
					'name' => "SlidersItemsRepeater[$id][SlidersItems][thumb_desktop]",
				]); ?>
            </td>
            <td>


                <label><?= \Yii::t('app', 'Изображение (адаптив)')?></label>
				<?= FileInput::widget([
					'model' => $model,
					'pluginOptions' => $fileInputPluginOptionsAdaptive,
					'attribute' => "[$id]thumb_adaptive",
					'options' => ['multiple' => false],
					'name' => "SlidersItemsRepeater[$id][SlidersItems][thumb_adaptive]",
				]); ?>
            </td>
        </tr>

        </tbody>
    </table>

<?php $this->endContent() ?>