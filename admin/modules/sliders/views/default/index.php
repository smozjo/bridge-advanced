<?php

use dosamigos\grid\GridView;
use yii\helpers\Html;
use yii2tech\admin\grid\ActionColumn;
use common\models\sliders\Sliders;
use yii\helpers\ArrayHelper;
use common\models\handbooks\Sections;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\sliders\search\SlidersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдеры';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    [
        'url' => ['index', Html::getInputName($searchModel, 'is_deleted') => !$searchModel->is_deleted],
        'label' => $searchModel->is_deleted ? \Yii::t('bridge', 'All records') : \Yii::t('bridge', 'Trash'),
        'icon' => $searchModel->is_deleted ? 'share-alt' : 'trash',
        'class' => 'btn btn-' . ($searchModel->is_deleted ? 'soft-info' : 'trash'),
    ],
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'searchTitle',
            'value' => function($data) {
                return $data->title;
            }
        ],

        [
            'attribute' => 'section_id',
            'value' => function(Sliders $model) {
                return $model->section->getTitle();
            },
            'filter' => Sections::getDropdownList('title', 'id', ['methods' => [
                ['currentTranslation']
            ]])
        ],

//        [
//            'attribute' => 'finished_at',
//            'value' => function(Sliders $model) {
//                return \Yii::$app->formatter->asDate($model->finished_at, 'php: d.m.Y H:i');
//            },
//            'filter' => DateTimePicker::widget([
//                'model' => $searchModel,
//                'attribute' => 'finished_at',
//                //  'options' => ['placeholder' => 'Enter birth date ...'],
//                'pluginOptions' => [
//                    'autoclose'=>true
//                ]
//            ]),
//            'format' => 'html'
//        ],
//        [
//            'attribute' => 'created_at',
//            'value' => function(Sliders $model) {
//                return \Yii::$app->formatter->asDate($model->created_at, 'php: d.m.Y H:i');
//            },
//            'filter' => DateTimePicker::widget([
//                'model' => $searchModel,
//                'attribute' => 'created_at',
//                //  'options' => ['placeholder' => 'Enter birth date ...'],
//                'pluginOptions' => [
//                    'autoclose'=>true
//                ]
//            ]),
//            'format' => 'html'
//        ],

        [
            'class' => 'yii2tech\admin\grid\PositionColumn',
            'value' => 'position',
            'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
            'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
        ],

        [
            'class' => \common\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
            'url' => ['toggle', 'type' => \Yii::$app->request->get('type')]
        ],

        [
            'class' => \common\components\admin\RFAActionColumn::class,
        ],
    ],
]); ?>
