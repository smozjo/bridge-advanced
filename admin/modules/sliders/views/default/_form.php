<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model common\models\sliders\Sliders */
/* @var $form Bridge\Core\Widgets\ActiveForm */

\common\assets\AjaxAsset::register($this);
?>

<?php $form = ActiveForm::begin(); ?>
<?php $form->field($model, 'image')->imageUpload()?>
<div class="row">

    <div class="col-md-12">

	<?= Tabs::widget([
		'id' => 'mainTabs',
		'items' => [
			[
				'label' => \Yii::t('app', 'Слайдер'),
				'active' => true,
				'content' => $this->render('_about-slider', [
					'form' => $form,
					'model' => $model
				])
			],
			[
				'label' => \Yii::t('app', 'Слайды'),
				'active' => false,
				'content' => $this->render('_slides', [
					'form' => $form,
					'model' => $model
				])
			]
		]
	]);
	?>


    </div>
</div>

    <div class="form-group">
		<?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
