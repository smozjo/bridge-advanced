<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\sliders\SlidersItems */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->field($model, 'slider_id')->textInput() ?>

        <?= $form->field($model, 'thumb_desktop')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'thumb_adaptive')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'user_id')->textInput() ?>

        <?= $form->field($model, 'is_active')->switchInput() ?>

        <?= $form->field($model, 'created_at')->dateTimePicker() ?>

        <?= $form->field($model, 'updated_at')->dateTimePicker() ?>


    </div>

    <div class="col-md-4">

    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
