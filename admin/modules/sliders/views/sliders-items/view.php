<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\sliders\SlidersItems */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sliders Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'slider_id',
            'thumb_desktop',
            'thumb_adaptive',
            'user_id',
            'position',
            'is_active',
            'created_at',
            'updated_at',
        ],
    ]) ?>
    </div>
</div>