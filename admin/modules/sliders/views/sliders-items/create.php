<?php

/* @var $this yii\web\View */
/* @var $model common\models\sliders\SlidersItems */

$this->title = 'Create Sliders Items';
$this->params['breadcrumbs'][] = ['label' => 'Sliders Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

