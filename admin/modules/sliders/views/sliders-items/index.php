<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel common\models\sliders\search\SlidersSearchItems */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sliders Items';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'slider_id',
        'thumb_desktop',
        'thumb_adaptive',
        'user_id',
        // [
            // 'class' => 'yii2tech\admin\grid\PositionColumn',
            // 'value' => 'position',
            // 'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
            // 'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
        // ],
        // [
            // 'class' => 'dosamigos\grid\columns\ToggleColumn',
            // 'attribute' => 'is_active',
            // 'onValue' => 1,
            // 'onLabel' => 'Active',
            // 'offLabel' => 'Not active',
            // 'contentOptions' => ['class' => 'text-center'],
            // 'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
            // 'filter' => ['1' => 'Active', '0' => 'Not active'],
        // ],
        // 'created_at',
        // 'updated_at',

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
