<?php

/* @var $this yii\web\View */
/* @var $model common\models\sliders\SlidersItems */

$this->title = 'Update Sliders Items: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sliders Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


