<?php

namespace admin\modules\sliders\controllers;

use common\core\CoreAdminController;
use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * SlidersItemsController implements the CRUD actions for [[common\models\sliders\SlidersItems]] model.
 * @see common\models\sliders\SlidersItems
 */
class SlidersItemsController extends CoreAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\sliders\SlidersItems';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'common\models\sliders\search\SlidersSearchItems';
    /**
     * @inheritdoc
     */
    public $allowedRoles = ['admin'];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'common\models\sliders\SlidersItems',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
