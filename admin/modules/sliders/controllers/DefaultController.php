<?php

namespace admin\modules\sliders\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use common\models\sliders\Sliders;
use common\models\sliders\SlidersItems;
use common\services\SliderService;
use Yii;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;
use yii2tech\admin\actions\Restore;
use yii2tech\admin\actions\SoftDelete;

/**
 * DefaultController implements the CRUD actions for [[common\models\sliders\Sliders]] model.
 * @see common\models\sliders\Sliders
 */
class DefaultController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\sliders\Sliders';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'common\models\sliders\search\SlidersSearch';
    /**
     * @inheritdoc
     */
    public $allowedRoles = ['admin'];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => Sliders::class,
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'items' => [
                    'class' => ToggleAction::class,
                    'modelClass' => SlidersItems::class,
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'delete' => [
                    'class' => SoftDelete::class,
                ],
                'restore' => [
                    'class' => Restore::class,
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }

    public function actionSubsection()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $section_id = $parents[0];
                return ['output'=>SliderService::getSectionItems($section_id), 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }
}
