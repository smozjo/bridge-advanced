<?php

namespace admin\modules\handbooks;

/**
 * handbooks module definition class
 */
class Module extends \yii\base\Module

{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'admin\modules\handbooks\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
