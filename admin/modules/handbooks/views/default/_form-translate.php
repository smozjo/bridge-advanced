<?php
/**
 * @var $languageCode string
 * @var $model app\modules\post\models\Post
 * @var $translationModel app\modules\post\models\PostTranslation
 * @var $form \Bridge\Core\Widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);
?>

<?= $form->field($translationModel, '['.$languageCode.']handbook_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '['.$languageCode.']lang')->hiddenInput()->label(false)  ?>
<?= $form->field($translationModel, '['.$languageCode.']title')->textInput(['disabled' => false]) ?>
<?= $form->field($translationModel, '['.$languageCode.']slug')->textInput(['disabled' => false]) ?>

<?php if($model::isDefaultColumn('description')): ?>
	<?= $form->field($translationModel, '['.$languageCode.']description')->richTextArea() ?>
<?php endif; ?>
