<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel common\models\handbooks\search\HandbooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \common\models\handbooks\Handbooks::getTypeTitle();
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create', 'type' => \Yii::$app->request->get('type')],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'searchTitle',
            'value' => function($data) {
                return $data->title;
            }
        ],
        [
            'class' => 'yii2tech\admin\grid\PositionColumn',
            'label' => 'Очередность',
            'value' => 'position',
            'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
            'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
        ],
        [
            'class' => \common\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],

        [
            'class' => \common\components\admin\RFAActionColumn::class,
            'excludeRemoveIds' => $searchModel::getExcludeRemoveIds(\Yii::$app->request->get('type')),
            'urlCreator' => function ($action, $model, $key, $index) {
                return \yii\helpers\Url::toRoute([$action, 'id' => $model->id, 'type' => \Yii::$app->request->get('type')]);
            }
        ],
    ],
]); ?>
