<?php

/* @var $this yii\web\View */
/* @var $model common\models\handbooks\Handbooks */

$this->title = 'Редактировать: ' . $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => \common\models\handbooks\Handbooks::getTypeTitle(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle()];
$this->params['breadcrumbs'][] = 'Редактировать';

$fotm = '';

if(is_file(\Yii::getAlias('@app/modules/handbooks/views/default/_form-' . \Yii::$app->request->get('type') . '.php'))) {
	$fotm = '-' . \Yii::$app->request->get('type');
}
?>

<?= $this->render('_form' . $fotm, [
    'model' => $model,
]) ?>


