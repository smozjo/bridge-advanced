<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\handbooks\Handbooks */
/* @var $form Bridge\Core\Widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

		<?= $form->translate($model, '@app/modules/handbooks/views/default/_form-translate') ?>

        <?= $form->field($model, 'type', ['options' => ['class' => 'hidden']])->hiddenInput([
                'value' => \Yii::$app->request->get('type')
        ]) ?>

		<?= $form->field($model, 'color')->widget(\zgb7mtr\colorPicker\ColorPicker::class) ?>

        <?= $form->field($model, 'is_active')->switchInput() ?>

		<?= $form->metaTags($model->metaTag) ?>


    </div>

    <div class="col-md-4">
        <?php if($model::isDefaultColumn('image')): ?>
		    <?= $form->field($model, 'image')->imageUpload() ?>
        <?php endif; ?>
    </div>
</div>

    <div class="form-group">
		<?= Html::submitButton(\Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
