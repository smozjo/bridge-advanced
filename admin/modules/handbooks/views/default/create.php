<?php

/* @var $this yii\web\View */
/* @var $model common\models\handbooks\Handbooks */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => \common\models\handbooks\Handbooks::getTypeTitle(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

