<?php

namespace admin\modules\handbooks\controllers;

use common\core\CoreAdminController;
use common\helpers\StatusHelper;
use common\models\handbooks\Handbooks;
use common\models\handbooks\Reason;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * DefaultController implements the CRUD actions for [[common\models\handbooks\Handbooks]] model.
 * @see common\models\handbooks\Handbooks
 */
class DefaultController extends CoreAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\handbooks\Handbooks';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'common\models\handbooks\search\HandbooksSearch';
    /**
     * @inheritdoc
     */
    public $allowedRoles = ['admin'];

    public function init()
	{
		$class = Handbooks::getClass();
		if($class) {
			$this->modelClass = $class;
		}
		parent::init();
	}

	public function behaviors()
	{
		$behaviors = parent::behaviors();

		$behaviors['access'] = [
			'class' => AccessControl::class,
			'rules' => [
				[
					'actions' => ['index', 'view', 'update', 'delete', 'create', 'toggle', 'position', 'reason-list'],
					'allow' => true,
					'roles' => ['admin'],
				],
				[
					'actions' => ['autofill'],
					'allow' => true,
					'roles' => ['admin'],
				],
			],
		];
		return $behaviors;
	}

	public function beforeAction($action)
	{
//		if($action->id === 'autofill') {
//			return parent::beforeAction($action);
//		}
//		if(!\Yii::$app->getUser()->can('editHandbooks', ['handbookTypeId' => \Yii::$app->request->get('type')])) {
//			throw new ForbiddenHttpException();
//		}
		return parent::beforeAction($action);
	}

	/**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'common\models\handbooks\Handbooks',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }

    public function actionReasonList()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents !== null) {
                $id = $parents[0];
            }
            return ['output' => '', 'selected' => ''];
        }
    }
}
