<?php

use yii\helpers\Url;
use kartik\depdrop\DepDrop;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View           $this
 * @var \Da\User\Model\User    $user
 * @var \Da\User\Model\Profile $profile
 * @var array $reasons
 */


$this->registerJsFile(
    'js/profile.js',
    ['depends' => [\yii\web\JqueryAsset::class]]
);

?>

<?php $this->beginContent('@Da/User/resources/views/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin(
	[
		'layout' => 'horizontal',
		'id' => 'profileForm',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'fieldConfig' => [
			'horizontalCssClasses' => [
				'wrapper' => 'col-sm-9',
			],
		],
	]
); ?>

<?php $this->beginContent('@admin/views/layouts/admin/_panel.php', ['title' => 'Основная информация']) ?>

    <?= $form->field($profile, 'name') ?>
    <?= $form->field($profile, 'public_email') ?>
    <?= $form->field($profile, 'website') ?>
    <?= $form->field($profile, 'location') ?>
    <?= $form->field($profile, 'gravatar_email') ?>
    <?= $form->field($profile, 'bio')->textarea() ?>

<?php $this->endContent() ?>

<div class="form-group">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('app', 'Обновить'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>