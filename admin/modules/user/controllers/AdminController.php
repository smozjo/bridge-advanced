<?php

namespace admin\modules\user\controllers;


use common\components\repeater\AppendAction;
use admin\modules\user\models\search\UserSearch;
use admin\modules\user\models\UsersChildren;
use common\helpers\StatusHelper;
use Da\User\Event\UserEvent;
use Da\User\Model\Profile;
use Da\User\Model\User;
use Da\User\Validator\AjaxRequestModelValidator;
use yii\helpers\ArrayHelper;

class AdminController extends \Da\User\Controller\AdminController
{

    public function actionUpdateProfile($id)
    {
        /** @var User $user */
        $user = $this->userQuery->where(['id' => $id])->one();
        /** @var Profile $profile */
        $profile = $user->profile;
        if ($profile === null) {
            $profile = $this->make(Profile::class);
            $profile->link('user', $user);
        }
        /** @var UserEvent $event */
        $event = $this->make(UserEvent::class, [$user]);

        $this->make(AjaxRequestModelValidator::class, [$profile])->validate();

        if ($profile->load(\Yii::$app->request->post())) {
            if ($profile->save()) {
                $this->trigger(UserEvent::EVENT_BEFORE_PROFILE_UPDATE, $event);
                \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Profile details have been updated'));
                $this->trigger(UserEvent::EVENT_AFTER_PROFILE_UPDATE, $event);

                return $this->refresh();
            }
        }

        $reasons = [];

        return $this->render(
            '@admin/modules/user/views/admin/_profile',
            [
                'user' => $user,
                'profile' => $profile,
                'reasons' => $reasons
            ]
        );
    }

}