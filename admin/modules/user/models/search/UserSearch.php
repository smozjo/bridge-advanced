<?php

namespace admin\modules\user\models\search;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class UserSearch extends \Da\User\Search\UserSearch
{
	public $blocked_at;
	public $confirmed_at;
	public $searchName;

    public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			[['blocked_at', 'confirmed_at', 'searchName'], 'safe']
		]);
	}

	public function search($params)
    {
        $query = $this->query
		->joinWith('profile');

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        if ($this->last_login_at !== null) {
            $date = strtotime($this->last_login_at);
            $query->andFilterWhere(['between', 'last_login_at', $date, $date + 3600 * 24]);
        }

        $query
            ->andFilterWhere(['OR',
				['like', 'username', $this->searchName],
				['like', 'profile.name', $this->searchName],
			])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['registration_ip' => $this->registration_ip]);

        if($this->blocked_at && (int)$this->blocked_at === 1) {
        	$query->andWhere(['IS NOT', 'blocked_at', null]);
		} elseif($this->blocked_at && (int)$this->blocked_at === 2) {
			$query->andWhere(['blocked_at' => null]);
		}

        if($this->confirmed_at && (int)$this->confirmed_at === 1) {
        	$query->andWhere(['IS NOT', 'confirmed_at', null]);
		} elseif($this->confirmed_at && (int)$this->confirmed_at === 2) {
			$query->andWhere(['confirmed_at' => null]);
		}

        return $dataProvider;
    }
}
