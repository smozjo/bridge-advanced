<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\pages\Pages */

$this->title = 'Просмотр: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'is_active',
                'format' => 'raw',
                'value' => function($data){
                    return $data->is_active ? '<span class="text-success">Показывается</span>' : '<span class="text-danger">Не показывается</span>';
                }
            ],
            'title',
            [
                'attribute' => 'slug',
                'format' => 'raw',
                'value' => function($data){
                    return '<a href="' . \Yii::$app->urlManagerFront->createUrl(['/page/' . $data->slug]) . '" target="_blank">' . \Yii::$app->urlManagerFront->createUrl(['/page/' . $data->slug]) . '</a>';
                }
            ],
            'text:html',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($model){
                    return '<img src="' . $model->getMainImage() . '">';
                }
            ],
        ],
    ]) ?>
    </div>
</div>