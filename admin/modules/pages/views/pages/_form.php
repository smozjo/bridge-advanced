<?php

use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\pages\Pages */
/* @var $form Bridge\Core\Widgets\ActiveForm */

$this->attachBehavior('CkePageBehavior', [
	'class' => \common\components\ckepage\CkePageBehavior::class,
	'form' => 'form',
	'model' => $model,
	'editorId' => 'pagestranslation-{{lang}}-text',
	'editableContentContainer' => '.content',
	'plugin' => 'kaztube-tools'
])

?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?php if(file_exists(\Yii::getAlias('@admin/modules/pages/views/pages/_form-translate-' . $model->id . '.php'))): ?>
		    <?= $form->translate($model, '@admin/modules/pages/views/pages/_form-translate-' . $model->id) ?>
        <?php else: ?>
		    <?= $form->translate($model, '@admin/modules/pages/views/pages/_form-translate') ?>
        <?php endif; ?>

		<?= $form->field($model, 'updated_at')->textInput(['disabled' => true]) ?>

		<?= $form->field($model, 'is_active')->switchInput() ?>

		<?= $form->metaTags($model->metaTag, '@app/../vendor/rocket-php-lab/yii2-bridge-core/src/views/meta-page/_form-meta-tags') ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'image')->imageUpload([
            'pluginOptions' => [
                'deleteUrl' => \yii\helpers\Url::to([
                    '/admin/base-admin/delete-file',
                    'id' => $model->id,
                    'modelName' => $model::className(),
                    'behaviorName' => 'image'
                ])
            ]
        ]); ?>
    </div>
</div>

    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>