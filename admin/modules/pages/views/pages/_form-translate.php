<?php
/**
 * @var $languageCode string
 * @var $model app\modules\post\models\Post
 * @var $translationModel app\modules\post\models\PostTranslation
 * @var $form \Bridge\Core\Widgets\ActiveForm
 */

$translationModel = $model->getTranslation($languageCode);



use yii\bootstrap\Html; ?>

<?= $form->field($translationModel, '['.$languageCode.']page_id')->hiddenInput()->label(false) ?>
<?= $form->field($translationModel, '['.$languageCode.']lang')->hiddenInput()->label(false)  ?>
<?= $form->field($translationModel, '['.$languageCode.']title')->textInput(['disabled' => false]) ?>
<?= $form->field($translationModel, '['.$languageCode.']subtitle')->textInput(['disabled' => false]) ?>
<?= $form->field($translationModel, '['.$languageCode.']slug')->textInput(['disabled' => false]) ?>

<?php


if(!trim(str_replace('&nbsp;', '', $translationModel->text))) {
	$translationModel->text = '<p>&nbsp;</p>';
}


$translationModel->text = <<<HTML
        <main class="b-main page-view">
            <div class="container">
                <div class="page-content content" id="content">
                    {$translationModel->text}
                </div>
            </div>
        </main>
HTML;

echo $form->field($translationModel, '['.$languageCode.']text')->richTextArea([
	'options' => ['disabled' => false],
	'preset' => 'full',
], [
	'extraPlugins' => 'colorbutton,font,justify,iframe,kaztube-tools',
	'removeButtons' => 'Paste,PasteText,PasteFromWord',
	'allowedContent' => true,
	'forcePasteAsPlainText' => true,
	'contentsCss' => [
		'/styles/main.min.css',
	]
])
?>

<!--?= $form->field($translationModel, '['.$languageCode.']seo_text')->richTextArea() ?-->


