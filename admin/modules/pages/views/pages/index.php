<?php

use dosamigos\grid\GridView;
use yii\helpers\Html;
use yii2tech\admin\grid\ActionColumn;
use common\models\pages\Pages;

/* @var $this yii\web\View */
/* @var $searchModel common\models\pages\search\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Текстовые страницы';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    [
        'url' => ['index', Html::getInputName($searchModel, 'is_deleted') => !$searchModel->is_deleted],
        'label' => $searchModel->is_deleted ? \Yii::t('bridge', 'All records') : \Yii::t('bridge', 'Trash'),
        'icon' => $searchModel->is_deleted ? 'share-alt' : 'trash',
        'class' => 'btn btn-' . ($searchModel->is_deleted ? 'soft-info' : 'trash'),
    ],
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'searchTitle',
            'value' => function(Pages $model){
                return $model->getTitle();
            }
        ],

        [
            'attribute' => 'searchSubTitle',
            'value' => function(Pages $model){
                return $model->getSubTitle();
            }
        ],

        'updated_at',

        [
            'class' => \common\components\admin\RFAToggleColumn::class,
            'attribute' => 'is_active',
        ],

        [
            'class' => \common\components\admin\RFAActionColumn::class,
            'template' => '{view}{update}{delete}'
        ],
    ],
]); ?>
