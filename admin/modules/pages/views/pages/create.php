<?php

/* @var $this yii\web\View */
/* @var $model common\models\pages\Pages */

$this->title = 'Добавление';
$this->params['breadcrumbs'][] = ['label' => 'Текстовые страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

