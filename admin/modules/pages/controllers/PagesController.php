<?php

namespace admin\modules\pages\controllers;

use common\core\CoreAdminController;
use common\models\pages\Pages;
use Bridge\Core\Controllers\BaseAdminController;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;
use yii2tech\admin\actions\Restore;
use yii2tech\admin\actions\SoftDelete;

/**
 * PagesController implements the CRUD actions for [[common\models\pages\Pages]] model.
 * @see common\models\pages\Pages
 */
class PagesController extends CoreAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\pages\Pages';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'common\models\pages\search\PagesSearch';
    /**
     * @inheritdoc
     */
    public $allowedRoles = ['admin'];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'common\models\pages\Pages',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'delete' => [
                    'class' => SoftDelete::class,
                ],
                'restore' => [
                    'class' => Restore::class,
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }

	public function behaviors()
	{
		$behaviors = parent::behaviors();

		$behaviors['access'] = [
			'class' => AccessControl::class,
			'rules' => [
				[
					'actions' => ['index', 'view', 'update', 'delete', 'restore', 'create', 'toggle', 'position', 'delete-gallery-file'],
					'allow' => true,
					'roles' => ['admin'],
				],

			],
		];
		return $behaviors;
	}

}
