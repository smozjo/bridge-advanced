class CkePage {
    constructor(form, editorIds, languages, editableContainer, externalPlugin) {
        this.form = form;
        this.languages = languages;
        this.editorIds = editorIds;
        this.editableContainer = editableContainer;
        this.externalPlugin = externalPlugin;
        this.init();
    }

    init() {
        const $self = this;

        if(this.externalPlugin) {
            CKEDITOR.plugins.addExternal(
                this.externalPlugin,
                '/js/ckeditor/plugin/',
                'plugin.js'
            );
        }
        $('body').on('beforeSubmit', $self.form, function () {
            $($self.languages).each(function (key, index) {
                $self.editorIds.forEach(function (item) {
                    let editor = CKEDITOR.instances[item.replace('{{lang}}', index)];
                    $(editor.getData()).each(function (index, value) {
                        var $container = $(value).find($self.editableContainer);
                        if ($container.length) {
                            console.log($container.html())
                            editor.setData($container.html());
                            return false; //break loop
                        }
                    });
                });
            });
        });
    }
}