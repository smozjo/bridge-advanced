/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 10.04.19
 * Time 16:04
 */

CKEDITOR.plugins.add( 'kaztube-tools', {
    init: function( editor ) {
        /**
         * H3 title
         */
        editor.addCommand( 'myH3Click', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                    ' <div class="contentble--subtitle">' +
                    '<h3>Заголовок</h3>' +
                    '</div>' );
            }
        });

        editor.ui.addButton( 'myH3',
            {
                label: 'Вставить подазголовок',
                command: 'myH3Click',
                toolbar: 'insert',
                icon: this.path + 'h3.png'
            } );

        /**
         * BlockQuote
         */
        editor.addCommand( 'myBlockquoteClick', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml( ' <blockquote class="blockquote"> ' +
                    '<div class="blockquote-content">' +
                    'Дом должен быть безопасным, комфортным, красивым. Основываясь на эти приципы, последние 25 лет мы строили дома с верой в то, что счастье клиентов - это и есть удовлетворенность и успех' +
                    '</div> ' +
                    '<span class="blockquote-content__author">Донгиль Тогын</span>' +
                    '</blockquote>' );
            }
        });

        editor.ui.addButton( 'myBlockquote',
            {
                label: 'Вставить цитату',
                command: 'myBlockquoteClick',
                toolbar: 'insert',
                icon: this.path + 'quote.png'
            } );

        /**
         * Quote2
         */
        editor.addCommand( 'myQuote2Click', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                '<div class="b-deviz">' +
                    '<p> Купить, арендовать, продать недвижимость с нами – это быстро, легко и культурно! </p>' +
                '</div>'
                );
            }
        });

        editor.ui.addButton( 'myQuote2',
            {
                label: 'Вставить вырезку в тексте',
                command: 'myQuote2Click',
                toolbar: 'insert',
                icon: this.path + 'quote_2.png'
            } );

        /**
         * myGalleryClick
         */
        editor.addCommand( 'myGalleryClick', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                    '<div id="slider">Место расположение слайдера</div>'
                );
            }
        });

        editor.ui.addButton( 'myGallery',
            {
                label: 'Вставить слайдер',
                command: 'myGalleryClick',
                toolbar: 'insert',
                icon: this.path + 'gallery.png'
            } );

        /**
         * Table
         */
        editor.addCommand( 'myTableClick', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                    '<table class="table"> ' +
                        '<thead> ' +
                            '<tr> ' +
                                '<th>№</th> ' +
                                '<th>Год</th> ' +
                            '</tr> ' +
                        '</thead> ' +
                        '<tbody> ' +
                        '<tr> ' +
                            '<td>1</td> ' +
                            '<td>2009</td> ' +
                        '</tr> ' +
                        '</tbody> ' +
                    '</table>'
                );
            }
        });

        /*editor.ui.addButton( 'myTable',
            {
                label: 'Вставить таблицу',
                command: 'myTableClick',
                toolbar: 'insert',
                icon: this.path + 'table.png'
            } );*/


         /**
         * myMarkList
         */
        editor.addCommand( 'myMarkListClick', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                    '<ul class="list"> ' +
                        '<li>Дом должен быть безопасным, комфортным, красивым.</li> ' +
                        '<li>Дом должен быть безопасным, комфортным, красивым.</li> ' +
                        '<li>Дом должен быть безопасным, комфортным, красивым.</li> ' +
                        '<li>Дом должен быть безопасным, комфортным, красивым.</li> ' +
                    '</ul>'
                );
            }
        });

        editor.ui.addButton( 'myMarkList',
            {
                label: 'Вставить марикированный список',
                command: 'myMarkListClick',
                toolbar: 'insert',
                icon: this.path + 'mark-list.png'
            } );

        /**
         * myNumList
         */
        editor.addCommand( 'myNumListClick', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                    '<ol class="list list--numeric"> ' +
                        '<li>Дом должен быть безопасным, комфортным, красивым.</li> ' +
                        '<li>Дом должен быть безопасным, комфортным, красивым.</li> ' +
                        '<li>Дом должен быть безопасным, комфортным, красивым.</li> ' +
                        '<li>Дом должен быть безопасным, комфортным, красивым.</li> ' +
                    '</ol>'
                );
            }
        });

        editor.ui.addButton( 'myNumList',
            {
                label: 'Вставить нумерованный список',
                command: 'myNumListClick',
                toolbar: 'insert',
                icon: this.path + 'num-list.png'
            } );

        /**
         * myPicture
         */
        editor.addCommand( 'myPictureClick', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                    '<figure><img src="/js/ckeditor/plugin/figcaption.jpg" alt=""/> ' +
                    '<figcaption>Дом должен быть безопасным, комфортным, красивым.</figcaption> ' +
                    '</figure>'
                );
            }
        });

        editor.ui.addButton( 'myPicture',
            {
                label: 'Вставить изображение',
                command: 'myPictureClick',
                toolbar: 'insert',
                icon: this.path + 'picture.png'
            } );

        /**
         * Text 1 column
         */
        editor.addCommand( 'myText1ColumnClick', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                    '<p class="text-column">Дом должен быть безопасным, комфортным, красивым.</p>'
                );
            }
        });

        editor.ui.addButton( 'myText1Column',
            {
                label: 'Вставить текст в одну колонку',
                command: 'myText1ColumnClick',
                toolbar: 'insert',
                icon: this.path + 'tex-1-column.png'
            } );

        /**
         * Text two columns
         */
        editor.addCommand( 'myTextTwoColumnClick', {
            exec: function( editor ) {
                var now = new Date();
                editor.insertHtml(
                    '<p class="text-column text-column--2">' +
                    'Дом должен быть безопасным, комфортным, красивым.' +
                    '</p>'
                );
            }
        });

        editor.ui.addButton( 'myTextTwoColumn',
            {
                label: 'Вставить текст в две колонки',
                command: 'myTextTwoColumnClick',
                toolbar: 'insert',
                icon: this.path + 'text-2-columns.png'
            } );
    }
});