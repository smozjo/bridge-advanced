var wAjax = function()
{
    var _options = {
        cursor: true,
        timeOut: 0,
        abort: true
    };
    var xhr;
    this.doit = function(action, data, callback, method, options){
        this.setOptions(options);

        if(_options.abort && xhr && xhr.readyState != 4 && xhr.readyState != 0){
            xhr.abort();
            this.setCursor('default');
        }
        if (_options.timeOut){
            setTimeout(
                this.post(action, data, method)
                , _options.timeOut);
        }
        else{
            this.post(action, data, callback, method);
        }
    };
    this.post = function(action, data, callback, method){
        if (_options.cursor !== false) {
            this.setCursor('wait');
        }
        else {
            _options.cursor = true;
        }
        xhr = jQuery.ajax({
            'type':(method == 'POST' ? method : 'GET'),
            'data':data,
            'url':'/'+action.replace(/^\/+|$/gm,''),
            'cache':false,

            'success':function(response){
                if(callback) {
                    callback(response);
                }

            }
        })
            .done(function() {
                if (_options.cursor !== false) {
                    var a = new wAjax();
                    a.setCursor('default');
                }
            })
            .fail(function() {
                if (_options.cursor !== false) {
                    var a = new wAjax();
                    a.setCursor('default');
                }
            })
        ;
    };
    this.setCursor = function (cursor) {
        jQuery('body').css('cursor', cursor);
    };
    this.setOptions = function(options) {
        if (typeof options == "undefined") {
            return false;
        }
        jQuery.each(_options, function (k, v) {
            if (typeof options[k] != "undefined") {
                _options[k] = options[k];
            }
        });
    };
};

var wjax = new wAjax();