<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=bridge_advanced',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'urlManagerAdmin' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'https://example.com/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
        ],
        'urlManagerFront' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'https://example.com/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
